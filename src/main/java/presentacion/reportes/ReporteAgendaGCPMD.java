package presentacion.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import persistencia.conexion.Conexion;

public class ReporteAgendaGCPMD
{
	private JasperReport reporte;
	private JasperViewer reporteViewer;
	private JasperPrint	reporteLleno;
	private Logger log = Logger.getLogger(ReporteAgendaGCPMD.class);
	//Recibe la lista de personas para armar el reporte
    public ReporteAgendaGCPMD()
    {
    	//Hardcodeado
		Map<String, Object> parametersMap = new HashMap<String, Object>();
		parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));	
		parametersMap.put("Imagen", new File("").getAbsolutePath()+"\\imagenes\\logoAgenda.png");	
    	try		{
    		File n = new File("");
            String dir = n.getAbsolutePath()+"\\reportes\\ReporteAgendaGroupCPMenosDatos.jrxml";         
            this.reporte = JasperCompileManager.compileReport(dir);
            this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap, 
					Conexion.getConexion().getSQLConexion());
    		log.info("Se carg� correctamente el reporte");
		}
		catch( JRException ex ) 
		{
			log.error("Ocurri� un error mientras se cargaba el archivo ReporteAgenda.Jasper", ex);
		}
    }       
    
    public void mostrar()
	{
		this.reporteViewer = new JasperViewer(this.reporteLleno,false);
		this.reporteViewer.setVisible(true);
	}
   
}	