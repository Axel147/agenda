package presentacion.vista;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


import dto.PaisDTO;
import dto.ProvinciaDTO;
import javafx.scene.control.ComboBox;

public class VentanaCargarLocalidad extends JFrame{

	private int tipoVentana;
	private int idLocalidadAModificar;
	private ComboBoxBoleanos comboBoxPaises;
	private ComboBoxBoleanos comboBoxProvincias;
	private JTextField txtNombreLocalidad;
	private JButton btnGuardarLocalidad;
	private JLabel lblProvinciaAsociada;
	private static VentanaCargarLocalidad INSTANCE;
	
	
	public static VentanaCargarLocalidad getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaCargarLocalidad(); 	
			return new VentanaCargarLocalidad();
		}
		else
			return INSTANCE;
	}
	
	public VentanaCargarLocalidad() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setBounds(100, 100, 388, 182);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		setResizable(false);
		
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JLabel lblPaisAsociado = new JLabel("Seleccione Pais");
		lblPaisAsociado.setBounds(21, 15, 131, 14);
		getContentPane().add(lblPaisAsociado);
		
		comboBoxPaises = new ComboBoxBoleanos();
		comboBoxPaises.setBounds(162, 11, 200, 22);
		getContentPane().add(comboBoxPaises);
		
		lblProvinciaAsociada = new JLabel("Seleccione provincia");
		lblProvinciaAsociada.setBounds(21, 48, 131, 14);
		getContentPane().add(lblProvinciaAsociada);
		
		comboBoxProvincias = new ComboBoxBoleanos();
		comboBoxProvincias.setBounds(162, 44, 200, 22);
		getContentPane().add(comboBoxProvincias);
		
		
		JLabel lblNombreProvincia = new JLabel("Nombre Localidad");
		lblNombreProvincia.setBounds(21, 82, 131, 14);
		getContentPane().add(lblNombreProvincia);
		
		txtNombreLocalidad = new JTextField();
		txtNombreLocalidad.setBounds(162, 77, 200, 20);
		getContentPane().add(txtNombreLocalidad);
		txtNombreLocalidad.setColumns(10);
		
		btnGuardarLocalidad = new JButton("Guardar");
		btnGuardarLocalidad.setBounds(259, 109, 89, 23);
		getContentPane().add(btnGuardarLocalidad);
	}

	
	
	public int getTipoVentana() {
		return tipoVentana;
	}

	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}

	public int getIdLocalidadAModificar() {
		return idLocalidadAModificar;
	}

	public void setIdLocalidadAModificar(int idLocalidadAModificar) {
		this.idLocalidadAModificar = idLocalidadAModificar;
	}

	public ComboBoxBoleanos getComboBoxPaises() {
		return comboBoxPaises;
	}

	public void setComboBoxPaises(ComboBoxBoleanos comboBoxPaises) {
		this.comboBoxPaises = comboBoxPaises;
	}

	public ComboBoxBoleanos getComboBoxProvincias() {
		return comboBoxProvincias;
	}

	public void setComboBoxProvincias(ComboBoxBoleanos comboBoxProvincias) {
		this.comboBoxProvincias = comboBoxProvincias;
	}

	public JTextField getTxtNombreLocalidad() {
		return txtNombreLocalidad;
	}

	public void setTxtNombreLocalidad(String txtNombreLocalidad) {
		this.txtNombreLocalidad.setText(txtNombreLocalidad);
	}

	public JButton getBtnGuardarLocalidad() {
		return btnGuardarLocalidad;
	}

	public void setBtnGuardarLocalidad(JButton btnGuardarLocalidad) {
		this.btnGuardarLocalidad = btnGuardarLocalidad;
	}

	public void llenarPais(List<PaisDTO> paises)
	{
		 this.comboBoxPaises.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.comboBoxPaises.removeAllItems();
            this.comboBoxPaises.setModel(cmb);
            for(PaisDTO pais: paises){
                cmb.addElement(pais);
            }
            this.comboBoxPaises.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los paises","Error",0);
        }
	}
	
	public void llenarProvincias(List<ProvinciaDTO> provincias)
	{
        this.comboBoxProvincias.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.comboBoxProvincias.removeAllItems();
            this.comboBoxProvincias.setModel(cmb);
            for(ProvinciaDTO provincia: provincias){
                cmb.addElement(provincia);
            }
            this.comboBoxProvincias.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las provincias" +ex,"Error",0);
        }
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public void advertenciaProvinciaNoSeleccionada() {
		JOptionPane.showMessageDialog(null,  "No se esta seleccionando ninguna provincia en el cual crear la localidad. \nEsto puede pasar debido a que no existe ninguna provincia cargada en el sistema. Cargue una nueva provincia para poder ingresar una localidad.","Cuidado",2);	
	}
	
	public void advertenciaFaltaNombreLocalidad(){
		JOptionPane.showMessageDialog(null,  "No se a ingresado ningun nombre para la localidad o el nombre esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);		
	}
	
	public void advertenciaLocalidadRepetida(){
		JOptionPane.showMessageDialog(null,  "No se pudo crear la localidad. \nRevise que no este creando una localidad repetida.","Falla al intentar crear la localidad",1);		
	}
	
	public void cerrar()
	{
		this.txtNombreLocalidad.setText(null);
		this.dispose();
	}
	
}
