package presentacion.vista;

import javax.swing.JComboBox;

public class ComboBoxBoleanos extends JComboBox {
	
	private boolean cargado;
	
	public boolean isLoaded(){
        return cargado;
    }
    
    public void setLoaded(boolean loaded){
        this.cargado = loaded;
    }
	
}
