package presentacion.vista;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


import dto.PaisDTO;
import dto.ProvinciaDTO;
import persistencia.conexion.Conexion;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class VentanaGeneralABMProvincia extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private static VentanaGeneralABMProvincia INSTANCE;
	private JTable tablaProvincias;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private ComboBoxBoleanos comboBoxListaPaises;
	private JLabel lblSeleccionePais;
	private DefaultTableModel modelProvincias;
	private  String[] nombreColumnas = {"Provincias"};
	
	
	
	public static VentanaGeneralABMProvincia getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaGeneralABMProvincia(); 	
			return new VentanaGeneralABMProvincia();
		}
		else
			return INSTANCE;
	}
	
	
	
	public VentanaGeneralABMProvincia() {
		super();
		initialize();
	}

	
	private void initialize() {
		
		setBounds(100, 100, 392, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		setResizable(false);
		
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setTitle("Personalizar Provincias");
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        dispose();
		    }
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 366, 261);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spProvincias = new JScrollPane();
		spProvincias.setToolTipText("");
		spProvincias.setBounds(10, 50, 346, 166);
		panel.add(spProvincias);
	
		modelProvincias = new DefaultTableModel(null,nombreColumnas);
		tablaProvincias = new JTable(modelProvincias);
		
		tablaProvincias.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaProvincias.getColumnModel().getColumn(0).setResizable(false);
		/*tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		*/
		spProvincias.setViewportView(tablaProvincias);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(10, 227, 89, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(139, 227, 89, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(267, 227, 89, 23);
		panel.add(btnBorrar);
		
		comboBoxListaPaises = new ComboBoxBoleanos();
		comboBoxListaPaises.setBounds(119, 11, 237, 23);
		panel.add(comboBoxListaPaises);
		
		lblSeleccionePais = new JLabel("Seleccione Pais");
		lblSeleccionePais.setBounds(10, 15, 109, 14);
		panel.add(lblSeleccionePais);
		
		
	}

	public void llenarPais(List<PaisDTO> paises)
	{
		this.comboBoxListaPaises.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.comboBoxListaPaises.removeAllItems();
            this.comboBoxListaPaises.setModel(cmb);
            for(PaisDTO pais: paises){
                cmb.addElement(pais);
            }
            this.comboBoxListaPaises.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los paises","Error",0);
        }
	}
	
	public void llenarTabla(List<ProvinciaDTO> listaProvincias) {
		this.getModelPais().setRowCount(0); //Para vaciar la tabla
		this.getModelPais().setColumnCount(0);
		this.getModelPais().setColumnIdentifiers(this.getNombreColumnas());

		for (ProvinciaDTO p : listaProvincias)
		{
			//String nombre = p.getNombre();
			Object[] fila = {p};
			this.getModelPais().addRow(fila);
		}
		
	}
	

	public JTable getTablaProvincias() {
		return tablaProvincias;
	}


	public void setTablaProvincias(JTable tablaProvincias) {
		this.tablaProvincias = tablaProvincias;
	}


	public JButton getBtnAgregar() {
		return btnAgregar;
	}


	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	

	public JButton getBtnEditar() {
		return btnEditar;
	}



	public void setBtnEditar(JButton btnEditar) {
		this.btnEditar = btnEditar;
	}



	public JButton getBtnBorrar() {
		return btnBorrar;
	}


	public void setBtnBorrar(JButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}


	public ComboBoxBoleanos getComboBoxListaPaises() {
		return comboBoxListaPaises;
	}


	public void setComboBoxListaPaises(ComboBoxBoleanos comboBoxListaPaises) {
		this.comboBoxListaPaises = comboBoxListaPaises;
	}


	public DefaultTableModel getModelPais() {
		return modelProvincias;
	}


	public void setModelPais(DefaultTableModel modelPais) {
		this.modelProvincias = modelPais;
	}


	public String[] getNombreColumnas() {
		return nombreColumnas;
	}


	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para editar o esta seleccionando mas de uno. \nPara editar porfavor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public void advertenciaProvinciaUtilizandose(){
		JOptionPane.showMessageDialog(null,  "No se pudo eliminar la provincia. \nRevise que no este siendo utilizada por algun contacto. De ser asi debe primero eliminar o modificar todos los contactos que la esten utilizando antes de eliminar la provincia.","Cuidado",2);		
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
}
