package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import dto.PersonaDTO;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import persistencia.conexion.Conexion;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;

import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;

import java.awt.Color;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;

public class Vista extends JFrame
{
	private JFrame frame;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private DefaultTableModel modelPersonas;
	private  String[] nombreColumnas = {"Nombre y apellido","Telefono","Email","Fecha de Nacimiento","Tipo de Contacto","Pais","Provincia","Localidad","Calle","Altura","Piso","Departamento","Ciudad Preferida"};
	private JPanel panel_1;

	public Vista() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		frame.setIconImage(imagen.getImage());
		frame.setTitle("Agenda");
		
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		frame.getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		
		JScrollPane spPersonas = new JScrollPane();
		panel.add(spPersonas);
		tablaPersonas = new JTable(modelPersonas);
		tablaPersonas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		
		spPersonas.setViewportView(tablaPersonas);
		
		panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setHgap(50);
		panel.add(panel_1, BorderLayout.SOUTH);
		
		btnAgregar = new JButton("Agregar");
		panel_1.add(btnAgregar);
		btnAgregar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnEditar = new JButton("Editar");
		panel_1.add(btnEditar);
		btnEditar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnBorrar = new JButton("Borrar");
		panel_1.add(btnBorrar);
		btnBorrar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnReporte = new JButton("Reporte");
		panel_1.add(btnReporte);
		btnReporte.setVerticalAlignment(SwingConstants.BOTTOM);
	}
	
	public void show()
	{
//		try{
//			UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	private void redefinirDimensionTabla(){
		JTable tabla = this.getTablaPersonas();
        for (int i = 0; i < tabla.getColumnCount(); i++) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) tabla.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            int width = 100;

            TableCellRenderer renderer = col.getHeaderRenderer();
            for (int r = 0; r < tabla.getRowCount(); r++) {
                renderer = tabla.getCellRenderer(r, i);
                Component comp = renderer.getTableCellRendererComponent(tabla, tabla.getValueAt(r, i),false, false, r, i);
                width = Math.max(width, comp.getPreferredSize().width);
            }
            col.setPreferredWidth(width + 25);
        }
    }

	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String mail = p.getMail();
			String fechaNac = p.getFechaNac();
			String tipoContacto = p.getTipoContacto().getValue();
			String pais = p.getDomicilio().getPais().getValue();
			String provincia = p.getDomicilio().getProvincia().getValue();
			String localidad = p.getDomicilio().getLocalidad().getValue();
			String calle = p.getDomicilio().getCalle();
			String altura = p.getDomicilio().getAltura();
			String piso = p.getDomicilio().getPiso();
			String departamento = p.getDomicilio().getDepartamento();
			String ciudadPreferida = p.getCiudadPreferida().getCiudadPreferida();
			Object[] fila = {nombre, tel, mail, fechaNac,tipoContacto, pais, provincia, localidad, calle, altura, piso, departamento, ciudadPreferida};
			this.getModelPersonas().addRow(fila);
			redefinirDimensionTabla();
		}
		
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para editar o esta seleccionando mas de uno. \nPara editar porfavor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public static void advertirFallaConexion(){
		JOptionPane.showMessageDialog(null,  "No se pudo inicializar la agenda debido a una falla de conexion. \nRevise que se este ejecutando el Wampserver o que la base de datos agenda exista.","Error",0);		
	}
}
