package presentacion.vista;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;


import dto.PaisDTO;
import persistencia.conexion.Conexion;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class VentanaCargarProvincia extends JFrame{
	private static final long serialVersionUID = 1L;
	private int tipoVentana;
	private int idProvinciaAModificar;
	private JTextField txtNombreProvincia;
	private JComboBox comboBoxPaises;
	private JButton btnGuardarProvincia;
	private static VentanaCargarProvincia INSTANCE;
	

	public static VentanaCargarProvincia getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaCargarProvincia(); 	
			return new VentanaCargarProvincia();
		}
		else
			return INSTANCE;
	}
	
	public VentanaCargarProvincia() {
		initialize();
	}
	
	
	private void initialize() {
		setBounds(100, 100, 334, 151);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		setResizable(false);
		
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JLabel lblPaisAsociado = new JLabel("Seleccione Pais");
		lblPaisAsociado.setBounds(22, 15, 125, 14);
		getContentPane().add(lblPaisAsociado);
		
		comboBoxPaises = new JComboBox();
		comboBoxPaises.setBounds(157, 11, 151, 22);
		getContentPane().add(comboBoxPaises);
		
		JLabel lblNombreProvincia = new JLabel("Nombre Provincia");
		lblNombreProvincia.setBounds(22, 52, 125, 14);
		getContentPane().add(lblNombreProvincia);
		
		txtNombreProvincia = new JTextField();
		txtNombreProvincia.setBounds(157, 49, 151, 20);
		getContentPane().add(txtNombreProvincia);
		txtNombreProvincia.setColumns(10);
		
		btnGuardarProvincia = new JButton("Guardar");
		btnGuardarProvincia.setBounds(219, 80, 89, 23);
		getContentPane().add(btnGuardarProvincia);
		
		this.setVisible(false);
	}
	
	public int getTipoVentana() {
		return tipoVentana;
	}

	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}

	public JTextField getTxtNombreProvincia() {
		return txtNombreProvincia;
	}


	public void setTxtNombreProvincia(String txtNombreProvincia) {
		this.txtNombreProvincia.setText(txtNombreProvincia);
	}


	public JComboBox getComboBoxPaises() {
		return comboBoxPaises;
	}


	public void setComboBoxPaises(JComboBox comboBoxPaises) {
		this.comboBoxPaises = comboBoxPaises;
	}


	public JButton getBtnGuardarProvincia() {
		return btnGuardarProvincia;
	}


	public void setBtnGuardarProvincia(JButton btnGuardarProvincia) {
		this.btnGuardarProvincia = btnGuardarProvincia;
	}
	
	
	
	public int getIdProvinciaAModificar() {
		return idProvinciaAModificar;
	}

	public void setIdProvinciaAModificar(int idProvinciaAModificar) {
		this.idProvinciaAModificar = idProvinciaAModificar;
	}

	public void llenarPais(List<PaisDTO> paises)
	{
		
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.comboBoxPaises.removeAllItems();
            this.comboBoxPaises.setModel(cmb);
            for(PaisDTO pais: paises){
                cmb.addElement(pais);
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los paises","Error",0);
        }
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public void advertenciaPaisNoSeleccionado() {
		JOptionPane.showMessageDialog(null,  "No se esta seleccionando ningun pais en el cual crear la provincia. \nEsto puede pasar debido a que no existe ningun pais cargado en el sistema. Cargue un nuevo pais para poder ingresar una provincia","Cuidado",2);
	}
	
	public void advertenciaFaltaNombreProvincia(){
		JOptionPane.showMessageDialog(null,  "No se a ingresado ningun nombre para la provincia o el nombre esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);		
	}
	
	public void advertenciaProvinciaRepetida(){
		JOptionPane.showMessageDialog(null,  "No se pudo crear la provincia. \nRevise que no este creando una provincia repetida.","Falla al intentar crear la provincia",1);		
	}
	
	public void cerrar()
	{
		this.txtNombreProvincia.setText(null);
		this.dispose();
	}
	
}
