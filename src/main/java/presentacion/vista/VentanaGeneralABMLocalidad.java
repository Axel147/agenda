package presentacion.vista;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.ProvinciaDTO;

public class VentanaGeneralABMLocalidad extends JFrame{

	private static final long serialVersionUID = 1L;
	private static VentanaGeneralABMLocalidad INSTANCE;
	private JTable tablaLocalidad;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JLabel lblSeleccionePais;
	private ComboBoxBoleanos comboBoxListaPaises;
	private JLabel lblSeleccioneProvincia;
	private ComboBoxBoleanos comboBoxListaProvincias;
	private DefaultTableModel modelLocalidad;
	private  String[] nombreColumnas = {"Localidades"};
	
	
	
	public static VentanaGeneralABMLocalidad getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaGeneralABMLocalidad(); 	
			return new VentanaGeneralABMLocalidad();
		}
		else
			return INSTANCE;
	}
	
	
	
	public VentanaGeneralABMLocalidad() {
		initialize();
	}

	
	private void initialize() {
		setBounds(100, 100, 414, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		setResizable(false);
		
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setTitle("Personalizar Localidades");
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        dispose();
		    }
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 388, 261);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spProvincias = new JScrollPane();
		spProvincias.setToolTipText("");
		spProvincias.setBounds(10, 79, 368, 137);
		panel.add(spProvincias);
	
		modelLocalidad = new DefaultTableModel(null,nombreColumnas);
		tablaLocalidad = new JTable(modelLocalidad);
		
		tablaLocalidad.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaLocalidad.getColumnModel().getColumn(0).setResizable(false);
		/*tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		*/
		spProvincias.setViewportView(tablaLocalidad);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(10, 227, 89, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(150, 227, 89, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(289, 227, 89, 23);
		panel.add(btnBorrar);
		
		lblSeleccionePais = new JLabel("Seleccione Pais");
		lblSeleccionePais.setBounds(10, 15, 130, 14);
		panel.add(lblSeleccionePais);
		
		comboBoxListaPaises = new ComboBoxBoleanos();
		comboBoxListaPaises.setBounds(150, 11, 228, 23);
		panel.add(comboBoxListaPaises);
		
		lblSeleccioneProvincia = new JLabel("Seleccione Provincia");
		lblSeleccioneProvincia.setBounds(10, 49, 130, 14);
		panel.add(lblSeleccioneProvincia);
		
		comboBoxListaProvincias = new ComboBoxBoleanos();
		comboBoxListaProvincias.setBounds(150, 45, 228, 23);
		panel.add(comboBoxListaProvincias);
		
	}

	
	public void llenarPais(List<PaisDTO> paises)
	{
		this.comboBoxListaPaises.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.comboBoxListaPaises.removeAllItems();
            this.comboBoxListaPaises.setModel(cmb);
            for(PaisDTO pais: paises){
                cmb.addElement(pais);
            }
            this.comboBoxListaPaises.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los paises","Error",0);
        }
	}
	
	public void llenarProvincias(List<ProvinciaDTO> provincias)
	{
		this.comboBoxListaProvincias.setLoaded(false);
		this.getModelLocalidad().setRowCount(0);// parche dos arreglar
//        this.comboBoxListaProvincias.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.comboBoxListaProvincias.removeAllItems();
            this.comboBoxListaProvincias.setModel(cmb);
            for(ProvinciaDTO provincia: provincias){
                cmb.addElement(provincia);
            }
            this.comboBoxListaProvincias.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las provincias","Error",0);
        }
	}
	
	public void llenarTabla(List<LocalidadDTO> listaLocalidades) {
		this.getModelLocalidad().setRowCount(0); //Para vaciar la tabla
		this.getModelLocalidad().setColumnCount(0);
		this.getModelLocalidad().setColumnIdentifiers(this.getNombreColumnas());

		for (LocalidadDTO p : listaLocalidades)
		{
			//String nombre = p.getNombre();
			Object[] fila = {p};
			this.getModelLocalidad().addRow(fila);
		}
		
	}
	

	public JTable getTablaProvincias() {
		return tablaLocalidad;
	}


	public void setTablaProvincias(JTable tablaProvincias) {
		this.tablaLocalidad = tablaProvincias;
	}


	public JButton getBtnAgregar() {
		return btnAgregar;
	}


	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	

	public JButton getBtnEditar() {
		return btnEditar;
	}



	public void setBtnEditar(JButton btnEditar) {
		this.btnEditar = btnEditar;
	}



	public JButton getBtnBorrar() {
		return btnBorrar;
	}


	public void setBtnBorrar(JButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}


	public ComboBoxBoleanos getComboBoxListaPaises() {
		return comboBoxListaPaises;
	}


	public void setComboBoxListaPaises(ComboBoxBoleanos comboBoxListaPaises) {
		this.comboBoxListaPaises = comboBoxListaPaises;
	}

	

	public ComboBoxBoleanos getComboBoxListaProvincias() {
		return comboBoxListaProvincias;
	}



	public void setComboBoxListaProvincias(ComboBoxBoleanos comboBoxListaProvincias) {
		this.comboBoxListaProvincias = comboBoxListaProvincias;
	}



	public DefaultTableModel getModelLocalidad() {
		return modelLocalidad;
	}


	public void setModelLocalidad(DefaultTableModel modelLocalidad) {
		this.modelLocalidad = modelLocalidad;
	}


	public String[] getNombreColumnas() {
		return nombreColumnas;
	}


	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para editar o esta seleccionando mas de uno. \nPara editar porfavor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public void advertenciaLocalidadUtilizandose(){
		JOptionPane.showMessageDialog(null,  "No se pudo eliminar la localidad. \nRevise que no este siendo utilizada por algun contacto. De ser asi debe primero eliminar o modificar todos los contactos que la esten utilizando antes de eliminar la localidad.","Cuidado",2);		
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
}
