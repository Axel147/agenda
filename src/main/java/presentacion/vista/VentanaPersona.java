package presentacion.vista;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import java.util.List;



import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;

import com.toedter.calendar.JDateChooser;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

public class VentanaPersona extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JButton btnPersonalizarTipoContacto;
	private JButton btnPersonalizarPais;
	private JButton btnPersonalizarProvincia;
	private JButton btnPersonalizarLocalidad;	
	private JButton btnGuardar;
	private static VentanaPersona INSTANCE;
	private JTextField txtMail;
	private JDateChooser fechaNac;
	private JComboBox tipoContacto;
	private JComboBox localidad;
	private ComboBoxBoleanos provincia;
	private ComboBoxBoleanos pais;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDepartamento;
	private int tipoVentana;
	private int idPersona;
	private JTextField txtCiudadPreferida;
//	private JTextField txtNumContacto;

	public static VentanaPersona getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPersona(); 	
			return new VentanaPersona();
		}
		else
			return INSTANCE;
	}
	
	private VentanaPersona() 
	{
		super();
		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 835, 274);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		setResizable(false);
		
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 809, 223);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(7, 14, 135, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(7, 74, 135, 14);
		panel.add(lblTelfono);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(152, 11, 261, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(152, 71, 261, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(699, 191, 101, 23);
		panel.add(btnGuardar);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setBounds(7, 101, 135, 14);
		panel.add(lblMail);
		
		txtMail = new JTextField();
		txtMail.setColumns(10);
		txtMail.setBounds(152, 98, 261, 20);
		panel.add(txtMail);
		
		JLabel lblCalle_1 = new JLabel("Calle");
		lblCalle_1.setBounds(423, 74, 127, 14);
		panel.add(lblCalle_1);
		
		txtCalle = new JTextField();
		txtCalle.setColumns(10);
		txtCalle.setBounds(560, 71, 240, 20);
		panel.add(txtCalle);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(423, 101, 127, 14);
		panel.add(lblAltura);
		
		txtAltura = new JTextField();
		txtAltura.setColumns(10);
		txtAltura.setBounds(560, 98, 86, 20);
		panel.add(txtAltura);
		
		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(673, 101, 30, 14);
		panel.add(lblPiso);
		
		txtPiso = new JTextField();
		txtPiso.setColumns(10);
		txtPiso.setBounds(713, 98, 86, 20);
		panel.add(txtPiso);
		
		JLabel lblDepartamento = new JLabel("Departamento ");
		lblDepartamento.setBounds(423, 132, 127, 14);
		panel.add(lblDepartamento);
		
		txtDepartamento = new JTextField();
		txtDepartamento.setColumns(10);
		txtDepartamento.setBounds(560, 129, 240, 20);
		panel.add(txtDepartamento);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(423, 43, 127, 14);
		panel.add(lblLocalidad);
		
		JLabel lblFechaDeNacimiento = new JLabel("Fecha de nacimiento");
		lblFechaDeNacimiento.setBounds(7, 129, 135, 14);
		panel.add(lblFechaDeNacimiento);
		
		JLabel lblTipoDeContacto = new JLabel("Tipo de contacto");
		lblTipoDeContacto.setBounds(7, 43, 135, 14);
		panel.add(lblTipoDeContacto);
		
		JLabel lblPais = new JLabel("Pais");
		lblPais.setBounds(7, 158, 135, 14);
		panel.add(lblPais);
		
		JLabel lblProvincia = new JLabel("Provincia");
		lblProvincia.setBounds(423, 14, 127, 14);
		panel.add(lblProvincia);
		
		tipoContacto = new JComboBox();
		tipoContacto.setBounds(152, 38, 213, 22);
		panel.add(tipoContacto);
		
		localidad = new JComboBox();
		localidad.setBounds(560, 38, 190, 22);
		panel.add(localidad);
		
		fechaNac = new JDateChooser();
		fechaNac.setBounds(152, 126, 261, 20);
		panel.add(fechaNac);
		
		provincia = new ComboBoxBoleanos();
		provincia.setBounds(560, 10, 190, 22);
		panel.add(provincia);
		
		pais = new ComboBoxBoleanos();
		pais.setBounds(152, 152, 213, 22);
		panel.add(pais);
		
		ImageIcon imagenConfig = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/config.png");
		imagenConfig = new ImageIcon(imagenConfig.getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		
		btnPersonalizarTipoContacto = new JButton("");
		btnPersonalizarTipoContacto.setIcon(imagenConfig);
		btnPersonalizarTipoContacto.setFont(new Font("Arial", Font.PLAIN, 8));
		btnPersonalizarTipoContacto.setBounds(375, 38, 38, 22);
		panel.add(btnPersonalizarTipoContacto);
		
		btnPersonalizarPais = new JButton("");
		btnPersonalizarPais.setIcon(imagenConfig);
		btnPersonalizarPais.setFont(new Font("Arial", Font.PLAIN, 8));
		btnPersonalizarPais.setBounds(375, 152, 38, 22);
		panel.add(btnPersonalizarPais);
		
		btnPersonalizarProvincia = new JButton("");
		btnPersonalizarProvincia.setIcon(imagenConfig);
		btnPersonalizarProvincia.setFont(new Font("Arial", Font.PLAIN, 8));
		btnPersonalizarProvincia.setBounds(761, 11, 38, 22);
		panel.add(btnPersonalizarProvincia);
		
		btnPersonalizarLocalidad = new JButton("");
		btnPersonalizarLocalidad.setIcon(imagenConfig);
		btnPersonalizarLocalidad.setFont(new Font("Arial", Font.PLAIN, 8));
		btnPersonalizarLocalidad.setBounds(761, 38, 38, 22);
		panel.add(btnPersonalizarLocalidad);
		
		JLabel lblCiudadPreferida = new JLabel("Ciudad Preferida");
		lblCiudadPreferida.setBounds(423, 157, 127, 14);
		panel.add(lblCiudadPreferida);
		
		txtCiudadPreferida = new JTextField();
		txtCiudadPreferida.setBounds(560, 155, 240, 20);
		panel.add(txtCiudadPreferida);
		txtCiudadPreferida.setColumns(10);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public int getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}
	
	public void setNombre(String Nombre) {
		this.txtNombre.setText(Nombre);
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}
	
	public void setTelefono(String Telefono) {
		this.txtTelefono.setText(Telefono);
	}
	
	public JButton getBtnAgregarPersona() 
	{
		return btnGuardar;
	}
	
	public int getTipoVentana() {
		return tipoVentana;
	}

	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}
	
	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JTextField getTxtMail() {
		return txtMail;
	}

	public void setMail(String Mail) {
		this.txtMail.setText(Mail);;
	}
	
	public JDateChooser getFechaNac() {
		return fechaNac;
	}

	public void setFechaNac(JDateChooser fechaNac) {
		this.fechaNac = fechaNac;
	}

	public JTextField getTxtCalle() {
		return txtCalle;
	}

	public void setCalle(String Calle) {
		this.txtCalle.setText(Calle);
	}

	public JTextField getTxtAltura() {
		return txtAltura;
	}

	public void setAltura(String Altura) {
		this.txtAltura.setText(Altura);
	}

	public JTextField getTxtPiso() {
		return txtPiso;
	}

	public void setPiso(String Piso) {
		this.txtPiso.setText(Piso);
	}

	public JTextField getTxtDepartamento() {
		return txtDepartamento;
	}

	public void setDepartamento(String Departamento) {
		this.txtDepartamento.setText(Departamento);
	}
	
	public JComboBox getTipoContacto()
	{
		return tipoContacto;
	}
	
	
	public ComboBoxBoleanos getPais() {
		return pais;
	}

	public ComboBoxBoleanos getProvincia()
	{
		return provincia;
	}
	
	public JComboBox getLocalidad()
	{
		return localidad;
	}
	
	public void setTipoContacto(JComboBox cmbTipoContacto)
	{
		this.tipoContacto = cmbTipoContacto;
	}
	
	public void setPais(ComboBoxBoleanos pais) {
		this.pais = pais;
	}
	
	public void setProvincia(ComboBoxBoleanos cmbProvincia)
	{
		this.provincia = cmbProvincia;
	}
	public void setLocalidad(JComboBox cmbLocalidad)
	{
		this.localidad = cmbLocalidad;
	}
	
	
	
	public JTextField getTxtCiudadPreferida() {
		return txtCiudadPreferida;
	}

	public void setTxtCiudadPreferida(String txtCiudadPreferida) {
		this.txtCiudadPreferida.setText(txtCiudadPreferida);
	}

	public JButton getBtnPersonalizarTipoContacto() {
		return btnPersonalizarTipoContacto;
	}

	public void setBtnPersonalizarTipoContacto(JButton btnPersonalizarTipoContacto) {
		this.btnPersonalizarTipoContacto = btnPersonalizarTipoContacto;
	}

	public JButton getBtnPersonalizarPais() {
		return btnPersonalizarPais;
	}

	public void setBtnPersonalizarPais(JButton btnPersonalizarPais) {
		this.btnPersonalizarPais = btnPersonalizarPais;
	}

	public JButton getBtnPersonalizarProvincia() {
		return btnPersonalizarProvincia;
	}

	public void setBtnPersonalizarProvincia(JButton btnPersonalizarProvincia) {
		this.btnPersonalizarProvincia = btnPersonalizarProvincia;
	}

	public JButton getBtnPersonalizarLocalidad() {
		return btnPersonalizarLocalidad;
	}

	public void setBtnPersonalizarLocalidad(JButton btnPersonalizarLocalidad) {
		this.btnPersonalizarLocalidad = btnPersonalizarLocalidad;
	}

	public void llenarTiposDeContacto(List<TipoContactoDTO> tiposDeContactos)
	{
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.tipoContacto.removeAllItems();
            this.tipoContacto.setModel(cmb);
            for(TipoContactoDTO tipoDeContacto: tiposDeContactos){
                cmb.addElement(tipoDeContacto);
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con tipo de contactos","Error",0);
        }
	}
	
	public void llenarPais(List<PaisDTO> paises)
	{
		this.pais.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.pais.removeAllItems();
            this.pais.setModel(cmb);
            for(PaisDTO pais: paises){
                cmb.addElement(pais);
            }
            this.pais.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los paises","Error",0);
        }
	}
	
	public void llenarProvincias(List<ProvinciaDTO> provincias)
	{
//		boolean comboBoxCargado = false;
		this.localidad.removeAllItems(); // parche cuidado arreglar
        this.provincia.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.provincia.removeAllItems();
            this.provincia.setModel(cmb);
//            System.out.println(provincias.toString());
            for(ProvinciaDTO provincia: provincias){
//            	System.out.println("pp: "+provincia);
                cmb.addElement(provincia);
            }
            this.provincia.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las provincias" +ex,"Error",0);
        }
	}
	
	public void llenarLocalidades(List<LocalidadDTO> localidades)
	{
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.localidad.removeAllItems();
            this.localidad.setModel(cmb);
            for(LocalidadDTO localidad: localidades){
                cmb.addElement(localidad);
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las localidades","Error",0);
        }
	}
	
	public void advertirFaltaMedioDeContacto()
	{
		JOptionPane.showMessageDialog(null,"Falta un medio de contacto (Email o Telefono). \nDebe establecer al menos un medio de contacto para registrar a una persona.","Cuidado",2);
	}
	
	public void advertirFaltaNombreContacto(){
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun nombre o el nombre esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
	}
	
	public void advertirFaltaItemListaDesplegable(){
		JOptionPane.showMessageDialog(null,"Una de las listas deplegables (Tipo de contato - Pais - Provincia - LocalidadDTO) no esta seleccionando ningun item. \nEsto puede pasar debido a que no existe ningun item de ese tipo. Cree un nuevo item para el tipo de dato faltante","Cuidado",2);
	}
	
	public void advertirEscrituraTelefono(){
		JOptionPane.showMessageDialog(null,"El telefono solo acepta caracteres n�mericos.","Caracter Invalido",2);
	}
	
	public void advertirEscrituraAltura(){
		JOptionPane.showMessageDialog(null,"La altura solo acepta caracteres n�mericos.","Caracter Invalido",2);
	}
	
	public void advertirEscrituraPiso(){
		JOptionPane.showMessageDialog(null,"El piso solo acepta caracteres n�mericos.","Caracter Invalido",2);
	}
	
	public void advertenciaPersonaRepetida(){
		JOptionPane.showMessageDialog(null,  "No se pudo agregar el nuevo contacto. \nRevise que no este queriendo agregar un contacto con un nombre repetido.","Falla al intentar agregar un contacto",1);		
	}
	
	public void advertirFaltaCiudadPreferida(){
		JOptionPane.showMessageDialog(null,"No se a ingresado ninguna ciudad preferida o el nombre de la ciudad preferida esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
	}
	
	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.txtMail.setText(null);
		this.fechaNac.setDate(null);
		this.txtCalle.setText(null);
		this.txtAltura.setText(null);
		this.txtPiso.setText(null);
		this.txtDepartamento.setText(null);
		this.dispose();
	}
}
