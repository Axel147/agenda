package presentacion.vista;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JPasswordField;


public class VentanaABMTerritorio extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JButton btnGuardar;
	private int tipoVentana;
	private static VentanaABMTerritorio INSTANCE;
	private int idElementoAModificar;


	public static VentanaABMTerritorio getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaABMTerritorio(); 	
			return new VentanaABMTerritorio();
		}
		else
			return INSTANCE;
	}
	

	private VentanaABMTerritorio() {
	
		
		super();
		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 297, 133);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		setResizable(false);
		
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 272, 82);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Nombre");
		lblNombreYApellido.setBounds(10, 11, 113, 14);
		panel.add(lblNombreYApellido);
		
		
		txtNombre = new JTextField();
		txtNombre.setBounds(69, 11, 190, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(158, 53, 101, 23);
		panel.add(btnGuardar);
		
		
		
		this.setVisible(false);
	}
	
	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(String txtNombre) {
		this.txtNombre.setText(txtNombre);
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	

	public int getIdElementoAModificar() {
		return idElementoAModificar;
	}


	public void setIdElementoAModificar(int idTipoContacto) {
		this.idElementoAModificar = idTipoContacto;
	}


	public int getTipoVentana() {
		return tipoVentana;
	}


	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}


	public void mostrarVentana() {
		this.setVisible(true);
		
	}

	public void advertirFaltaNombreTipoContacto() {
		JOptionPane.showMessageDialog(null,  "No se a ingresado ningun nombre para el tipo de contacto o el nombre esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
		
	}
	
	public void advertirFaltaNombrePais() {
		JOptionPane.showMessageDialog(null,  "No se a ingresado ningun nombre para el pais o el nombre esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
	}
	
	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.dispose();
	}
}
