package presentacion.vista;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

public class VentanaABMTipoContacto extends JFrame{
	private static final long serialVersionUID = 1L;
//	private JFrame frame;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private static VentanaABMTipoContacto INSTANCE;
	private DefaultTableModel modelPersonas;
	private  String[] nombreColumnas = {"Tipos de Contacto"};
	private JPanel panel_1;

	
	public static VentanaABMTipoContacto getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaABMTipoContacto(); 	
			return new VentanaABMTipoContacto();
		}
		else
			return INSTANCE;
	}
	
	
	
	private VentanaABMTipoContacto() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		new JFrame();
		setBounds(100, 100, 328, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setTitle("Personalizar Tipos de Contacto");
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        dispose();
		    }
		});
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane spPersonas = new JScrollPane();
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		/*tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		*/
		spPersonas.setViewportView(tablaPersonas);
		
		panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setHgap(35);
		panel.add(panel_1, BorderLayout.SOUTH);
		
		btnAgregar = new JButton("Agregar");
		panel_1.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		panel_1.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		panel_1.add(btnBorrar);
		
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para editar o esta seleccionando mas de uno. \nPara editar porfavor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public void advertenciaContactoUtilizandose(){
		JOptionPane.showMessageDialog(null,  "No se pudo eliminar el tipo de contacto. \nRevise que no este siendo utilizado por algun contacto. De ser asi debe primero eliminar o modificar todos los contactos que lo esten utilizando antes de eliminar el tipo de contacto.","Cuidado",2);		
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}

	public void llenarTabla(List<TipoContactoDTO> tiposContacto) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (TipoContactoDTO p : tiposContacto)
		{
			//String nombre = p.getNombre();
			Object[] fila = {p};
			this.getModelPersonas().addRow(fila);
		}
		
	}



}