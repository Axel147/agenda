package presentacion.vista;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import java.util.List;



import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;

import com.toedter.calendar.JDateChooser;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

public class VentanaReporte extends JFrame {
	private static final long serialVersionUID = 1L;
	private static VentanaReporte INSTANCE;
	private JPanel panel;
	private JRadioButton rdbtnReporteGroupCP;
	private JRadioButton rdbtnReporteGroupCPDM;
	private JRadioButton rdbtnReporteGroupCPPaPrL;
	private JRadioButton rdbtnReporteAgenda;
	private JButton btnGenerarReportes;
	private JButton btnCancelar;
	private JPanel contentPane;

	public static VentanaReporte getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaReporte(); 	
			return new VentanaReporte();
		}
		else
			return INSTANCE;
	}
	
	private VentanaReporte() 
	{
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 762, 247);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		setTitle("Generar Reportes");
		setResizable(false);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JPanel panel = new JPanel();
//		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
//		setContentPane(panel);
//		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setBounds(10, 11, 736, 203);
		contentPane.add(panel);
		panel.setLayout(null);
		
		rdbtnReporteGroupCP = new JRadioButton("Reporte Agrupado Por Ciudad Preferida");
		rdbtnReporteGroupCP.setBounds(10, 67, 525, 23);
		panel.add(rdbtnReporteGroupCP);
		
		rdbtnReporteGroupCPDM = new JRadioButton("Reporte Agrupado por Ciudad Preferida Detalles Minimos");
		rdbtnReporteGroupCPDM.setBounds(10, 93, 525, 23);
		panel.add(rdbtnReporteGroupCPDM);
		
		rdbtnReporteGroupCPPaPrL = new JRadioButton("Reporte Agrupado por Ciudad Preferida con SubGrupos Pais - Provincia - Localidad");
		rdbtnReporteGroupCPPaPrL.setBounds(10, 119, 525, 23);
		panel.add(rdbtnReporteGroupCPPaPrL);
		
		rdbtnReporteAgenda = new JRadioButton("Reporte Agenda");
		rdbtnReporteAgenda.setBounds(10, 41, 525, 23);
		panel.add(rdbtnReporteAgenda);
		
		JLabel lblSeleccionesLosReportes = new JLabel("Selecciones los reportes que quiere generar:");
		lblSeleccionesLosReportes.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblSeleccionesLosReportes.setBounds(10, 11, 263, 23);
		panel.add(lblSeleccionesLosReportes);
		
		btnGenerarReportes = new JButton("Generar Reportes");
		btnGenerarReportes.setBounds(461, 172, 165, 23);
		panel.add(btnGenerarReportes);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(637, 172, 89, 23);
		panel.add(btnCancelar);
		
		setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JRadioButton getRdbtnReporteGroupCP() {
		return rdbtnReporteGroupCP;
	}




	public void setRdbtnReporteGroupCP(JRadioButton rdbtnReporteGroupCP) {
		this.rdbtnReporteGroupCP = rdbtnReporteGroupCP;
	}




	public JRadioButton getRdbtnReporteGroupCPDM() {
		return rdbtnReporteGroupCPDM;
	}




	public void setRdbtnReporteGroupCPDM(JRadioButton rdbtnReporteGroupCPDM) {
		this.rdbtnReporteGroupCPDM = rdbtnReporteGroupCPDM;
	}




	public JRadioButton getRdbtnReporteGroupCPPaPrL() {
		return rdbtnReporteGroupCPPaPrL;
	}




	public void setRdbtnReporteGroupCPPaPrL(JRadioButton rdbtnReporteGroupCPPaPrL) {
		this.rdbtnReporteGroupCPPaPrL = rdbtnReporteGroupCPPaPrL;
	}




	public JRadioButton getRdbtnReporteAgenda() {
		return rdbtnReporteAgenda;
	}




	public void setRdbtnReporteAgenda(JRadioButton rdbtnReporteAgenda) {
		this.rdbtnReporteAgenda = rdbtnReporteAgenda;
	}

	


	public JButton getBtnGenerarReportes() {
		return btnGenerarReportes;
	}

	public void setBtnGenerarReportes(JButton btnGenerarReportes) {
		this.btnGenerarReportes = btnGenerarReportes;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public void cerrar()
	{
		this.rdbtnReporteAgenda.setSelected(false);
		this.rdbtnReporteGroupCP.setSelected(false);
		this.rdbtnReporteGroupCPDM.setSelected(false);
		this.rdbtnReporteGroupCPPaPrL.setSelected(false);
		this.dispose();
	}
}
