package presentacion.vista;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


import dto.PaisDTO;
import persistencia.conexion.Conexion;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

public class VentanaGeneralABMPais extends JFrame{
	private static final long serialVersionUID = 1L;
	private JTable tablaPais;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private static VentanaGeneralABMPais INSTANCE;
	private DefaultTableModel modelPais;
	private  String[] nombreColumnas = {"Paises"};

	
	public static VentanaGeneralABMPais getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaGeneralABMPais(); 	
			return new VentanaGeneralABMPais();
		}
		else
			return INSTANCE;
	}
	
	
	
	private VentanaGeneralABMPais() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		setBounds(100, 100, 328, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setTitle("Personalizar Paises");
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        dispose();
		    }
		});
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane spPersonas = new JScrollPane();
		panel.add(spPersonas);
		
		modelPais = new DefaultTableModel(null,nombreColumnas);
		tablaPais = new JTable(modelPais);
		
		tablaPais.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPais.getColumnModel().getColumn(0).setResizable(false);
		/*tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		*/
		spPersonas.setViewportView(tablaPais);
		
		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setHgap(35);
		panel.add(panel_1, BorderLayout.SOUTH);
		
		btnAgregar = new JButton("Agregar");
		panel_1.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		panel_1.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		panel_1.add(btnBorrar);
		
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPais;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPais;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para editar o esta seleccionando mas de uno. \nPara editar porfavor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public void advertenciaPaisUtilizandose(){
		JOptionPane.showMessageDialog(null,  "No se pudo eliminar el pais. \nRevise que no este siendo utilizado por algun contacto. De ser asi debe primero eliminar o modificar todos los contactos que lo esten utilizando antes de eliminar el pais.","Cuidado",2);		
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}

	public void llenarTabla(List<PaisDTO> paises) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PaisDTO p : paises)
		{
			//String nombre = p.getNombre();
			Object[] fila = {p};
			this.getModelPersonas().addRow(fila);
		}
		
	}
}