package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Date;
import java.util.EventListener;
import java.util.List;
import java.util.regex.Pattern;

import modelo.*;
import presentacion.reportes.ReporteAgenda;
import presentacion.reportes.ReporteAgendaGCP;
import presentacion.reportes.ReporteAgendaGCPMD;
import presentacion.reportes.ReporteAgendaGCPaPrL;
import presentacion.vista.VentanaABMTipoContacto;
import presentacion.vista.VentanaCargarLocalidad;
import presentacion.vista.VentanaCargarProvincia;
import presentacion.vista.VentanaGeneralABMLocalidad;
import presentacion.vista.VentanaGeneralABMPais;
import presentacion.vista.VentanaGeneralABMProvincia;
import presentacion.vista.VentanaABMTerritorio;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaReporte;
import presentacion.vista.Vista;
import dto.CiudadPreferidaDTO;
import dto.DomicilioDTO;
import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;
import javafx.util.Pair;

public class Controlador implements /*ActionListener,*/ EventListener
{
		private Vista vista;
		private List<PersonaDTO> personasEnTabla;
		private List<TipoContactoDTO> tiposContactos;
		private List<PaisDTO> paises;
		private List<ProvinciaDTO> provincias;
		private List<LocalidadDTO> localidades;
		private VentanaPersona ventanaPersona; 
		private VentanaABMTipoContacto ventanaABMTipoContacto;
		private VentanaABMTerritorio ventanaABMTerritorio;
		private VentanaCargarProvincia ventanaCargarProvincia;
		private VentanaCargarLocalidad ventanaCargarLocalidad;
		private VentanaGeneralABMPais ventanaGeneralABMPais;
		private VentanaGeneralABMProvincia ventanaGeneralABMProvincia;
		private VentanaGeneralABMLocalidad ventanaGeneralABMLocalidad;
		private VentanaReporte ventanaReporte;
		private TipoContacto tipoContacto;
		private Agenda agenda;
//		private ListaDesplegable listaDesplegable;
		private Pais pais;
		private Provincia provincia;
		private Localidad localidad;
		
		
		public Controlador(Vista vista, Agenda agenda, TipoContacto tipoContacto, Pais pais, Provincia provincia, Localidad localidad)
		{
			//se cargan las clases de tipo modelo
			this.agenda = agenda;
//			this.listaDesplegable = listaDesplegable;
			this.tipoContacto = tipoContacto;
			this.pais = pais;
			this.provincia = provincia;
			this.localidad = localidad;
			
			//se cargan las clases jframe
			this.vista = vista;
			this.ventanaPersona = VentanaPersona.getInstance();
			this.ventanaABMTipoContacto = VentanaABMTipoContacto.getInstance();
			this.ventanaGeneralABMPais = VentanaGeneralABMPais.getInstance();
			this.ventanaGeneralABMProvincia = VentanaGeneralABMProvincia.getInstance();
			this.ventanaGeneralABMLocalidad = VentanaGeneralABMLocalidad.getInstance();
			this.ventanaABMTerritorio = VentanaABMTerritorio.getInstance();
			this.ventanaCargarProvincia = VentanaCargarProvincia.getInstance();
			this.ventanaCargarLocalidad = VentanaCargarLocalidad.getInstance();
			this.ventanaReporte = VentanaReporte.getInstance();
			
			
			
			//se crean eventos para los elementos del jframe vista
			this.vista.getBtnAgregar().addActionListener(a->ventanaAgregarPersona(a));
			this.vista.getBtnEditar().addActionListener(m->ventanaEditarPersona(m));
			this.vista.getBtnBorrar().addActionListener(s->borrarPersona(s));
			this.vista.getBtnReporte().addActionListener(r->mostrarReporte(r));
			
			//se crean eventos para los elementos del jframe ventanaPersona
			this.ventanaPersona.getBtnAgregarPersona().addActionListener(p->guardarPersona(p));
			this.ventanaPersona.getPais().addItemListener(ps->actualizarProvincia(ps));
			this.ventanaPersona.getProvincia().addItemListener(pr->actualizarLocalidad(pr));
			this.ventanaPersona.getBtnPersonalizarTipoContacto().addActionListener(ptc->mostrarABMTipoContacto(ptc));
			this.ventanaPersona.getBtnPersonalizarPais().addActionListener(pp->mostrarABMPais(pp));
			this.ventanaPersona.getBtnPersonalizarProvincia().addActionListener(pprov->mostrarABMProvincia(pprov));
			this.ventanaPersona.getBtnPersonalizarLocalidad().addActionListener(ploc->mostrarABMLocalidad(ploc));
			this.ventanaPersona.getTxtTelefono().addKeyListener(new KeyListener(){@Override public void keyTyped(KeyEvent e) {revisarEscrituraTelefono(e);} @Override public void keyPressed(KeyEvent e){} @Override public void keyReleased(KeyEvent e){}});
			this.ventanaPersona.getTxtAltura().addKeyListener(new KeyListener(){@Override public void keyTyped(KeyEvent e) {revisarEscrituraAltura(e);} @Override public void keyPressed(KeyEvent e){} @Override public void keyReleased(KeyEvent e){}});
			this.ventanaPersona.getTxtPiso().addKeyListener(new KeyListener(){@Override public void keyTyped(KeyEvent e) {revisarEscrituraPiso(e);} @Override public void keyPressed(KeyEvent e){} @Override public void keyReleased(KeyEvent e){}});
			this.ventanaPersona.getTxtCiudadPreferida().addKeyListener(new KeyListener(){@Override public void keyTyped(KeyEvent e) {} @Override public void keyPressed(KeyEvent e){} @Override public void keyReleased(KeyEvent e){revisarEscrituraCiudadPreferida(e);}});
			
			//Se crean eventos para los elementos del jframe ventanaABMTipoContacto
			this.ventanaABMTipoContacto.getBtnAgregar().addActionListener(agtc->agregarTipoContacto(agtc));
			this.ventanaABMTipoContacto.getBtnEditar().addActionListener(etc->editarTipoContacto(etc));
			this.ventanaABMTipoContacto.getBtnBorrar().addActionListener(btc->borrarTipoContacto(btc));
			
			//Se crean eventos para los elementos del jframe ventanaGeneralABMPais
			this.ventanaGeneralABMPais.getBtnAgregar().addActionListener(agp->agregarPais(agp));
			this.ventanaGeneralABMPais.getBtnEditar().addActionListener(ep->editarPais(ep));
			this.ventanaGeneralABMPais.getBtnBorrar().addActionListener(bp->borrarPais(bp));
			
			//Se crean eventos para los elementos del jframe ventanaGeneralABMProvincia
			this.ventanaGeneralABMProvincia.getBtnAgregar().addActionListener(agprov->agregarProvincia(agprov));
			this.ventanaGeneralABMProvincia.getBtnEditar().addActionListener(eprov->editarProvincia(eprov));
			this.ventanaGeneralABMProvincia.getBtnBorrar().addActionListener(bprov->borrarProvincia(bprov));
			this.ventanaGeneralABMProvincia.getComboBoxListaPaises().addItemListener(acprov->actualizarTblProvincia(acprov));
			
			//Se crean eventos para los elementos del jframe ventanaGeneralABMLocalidad
			this.ventanaGeneralABMLocalidad.getBtnAgregar().addActionListener(agl->agregarLocalidad(agl));
			this.ventanaGeneralABMLocalidad.getBtnEditar().addActionListener(el->editarLocalidad(el));
			this.ventanaGeneralABMLocalidad.getBtnBorrar().addActionListener(bl->borrarLocalidad(bl));
			this.ventanaGeneralABMLocalidad.getComboBoxListaPaises().addItemListener(acps->actualizarCmbProvincia(acps));
			this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().addItemListener(acprov->actualizarTblLocalidades(acprov));
			
			//se crean eventos para los elementos del jframe ventanaABMTerritorio
			this.ventanaABMTerritorio.getBtnGuardar().addActionListener(gtc->guardarContacto(gtc));
			
			//se crean eventos para los elementos del jframe ventanaAgregarProvincia
			this.ventanaCargarProvincia.getBtnGuardarProvincia().addActionListener(gp->guardarProvincia(gp));
			
			//se crean eventos para los elementos del jframe ventanaAgregarLocalidad
			this.ventanaCargarLocalidad.getBtnGuardarLocalidad().addActionListener(gl->guardarLocalidad(gl));
			this.ventanaCargarLocalidad.getComboBoxPaises().addItemListener(acpsl->actualizarCmbProvinciaEnCargarLocalidad(acpsl));
			
			//se crean los eventos para los elementos del jframe ventanaReporte
			this.ventanaReporte.getBtnCancelar().addActionListener(cr->cancelarReportes(cr));
			this.ventanaReporte.getBtnGenerarReportes().addActionListener(gr->generarReportes(gr));
		}

		//evento que abre el jframe 'ventanaPersona' para agregar una persona
		private void ventanaAgregarPersona(ActionEvent a) {
			this.ventanaPersona.setTipoVentana(1);
			this.ventanaPersona.setTitle("Agregar Contacto");
			cargarListasDesplegables();
			this.ventanaPersona.mostrarVentana();
		}
		
		//evento que abre el jframe 'ventanaPersona' para editar una persona
		public void ventanaEditarPersona(ActionEvent m)
		{
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			if(filasSeleccionadas.length == 1)//verificamos que seleccionara solo un valor para editar de la tabla personas
			{
				for (int fila : filasSeleccionadas)
				{
					this.ventanaPersona.setTipoVentana(2); //setea tipo ventana editar
					this.ventanaPersona.setTitle("Editar Contacto");
					cargarListasDesplegables();
					//seteamos datos de la persona a editar
//					this.ventanaPersona.setTxtNumContacto(String.valueOf(this.personasEnTabla.get(fila).getIdPersona()));
					this.ventanaPersona.setIdPersona(this.personasEnTabla.get(fila).getIdPersona());
					this.ventanaPersona.setNombre(this.personasEnTabla.get(fila).getNombre());
					this.ventanaPersona.getTipoContacto().setSelectedItem(new TipoContactoDTO(this.personasEnTabla.get(fila).getTipoContacto().getKey(), this.personasEnTabla.get(fila).getTipoContacto().getValue()));
					this.ventanaPersona.setTelefono(this.personasEnTabla.get(fila).getTelefono());
					this.ventanaPersona.setMail(this.personasEnTabla.get(fila).getMail());
					if(!this.personasEnTabla.get(fila).getFechaNac().equals("") && !this.personasEnTabla.get(fila).getFechaNac().equals(null)){
						String[] splitFechaNac = this.personasEnTabla.get(fila).getFechaNac().split("-");//separamos el string de fecha para ingresarlo luego al JDateChooser
	                    this.ventanaPersona.getFechaNac().setDate(new Date(Integer.parseInt(splitFechaNac[0])-1900,Integer.parseInt(splitFechaNac[1])-1,Integer.parseInt(splitFechaNac[2])));		
					}
					this.ventanaPersona.getPais().setSelectedItem(new PaisDTO(this.personasEnTabla.get(fila).getDomicilio().getPais().getKey(), this.personasEnTabla.get(fila).getDomicilio().getPais().getValue()));
					this.ventanaPersona.getProvincia().setSelectedItem(new ProvinciaDTO(this.personasEnTabla.get(fila).getDomicilio().getProvincia().getKey(), this.personasEnTabla.get(fila).getDomicilio().getProvincia().getValue()));
					this.ventanaPersona.getLocalidad().setSelectedItem(new LocalidadDTO(this.personasEnTabla.get(fila).getDomicilio().getLocalidad().getKey(), this.personasEnTabla.get(fila).getDomicilio().getLocalidad().getValue()));
					this.ventanaPersona.setCalle(this.personasEnTabla.get(fila).getDomicilio().getCalle());
					this.ventanaPersona.setAltura(this.personasEnTabla.get(fila).getDomicilio().getAltura());
					this.ventanaPersona.setPiso(this.personasEnTabla.get(fila).getDomicilio().getPiso());
					this.ventanaPersona.setDepartamento(this.personasEnTabla.get(fila).getDomicilio().getDepartamento());
					this.ventanaPersona.setTxtCiudadPreferida(this.personasEnTabla.get(fila).getCiudadPreferida().getCiudadPreferida());
					this.ventanaPersona.mostrarVentana();
				}
			}else this.vista.advertirSeleccionIncorrecta();
			this.refrescarTabla();
		}
		
		private void mostrarABMTipoContacto(ActionEvent ptc) {
			this.ventanaABMTipoContacto.mostrarVentana();
			refrescarTablaTipoDeContacto();
		}
		
		private void agregarTipoContacto(ActionEvent agtc) {
			this.ventanaABMTerritorio.setTipoVentana(1);
			this.ventanaABMTerritorio.setTitle("Agregar Tipo de Contacto");
			this.ventanaABMTerritorio.mostrarVentana();
		}
		
		//cambia el nombre de este metodo
		private void guardarContacto(ActionEvent gtc) {
			String regexp = "^$||\\s+$";
			boolean isNombreInvalido= false;
			if(Pattern.matches(regexp, this.ventanaABMTerritorio.getTxtNombre().getText()))isNombreInvalido = true;
			if(this.ventanaABMTerritorio.getTipoVentana()==1) {
				if(isNombreInvalido)this.ventanaABMTerritorio.advertirFaltaNombreTipoContacto();
				else {
					String nombre = this.ventanaABMTerritorio.getTxtNombre().getText();
					this.tipoContacto.agregarTipoDeContacto(nombre);
					this.refrescarTablaTipoDeContacto();
					this.tiposContactos = tipoContacto.obtenerTiposContacto();
					this.ventanaPersona.llenarTiposDeContacto(this.tiposContactos);
				}
			}else if(this.ventanaABMTerritorio.getTipoVentana()==2) {
				if(isNombreInvalido)this.ventanaABMTerritorio.advertirFaltaNombreTipoContacto();
				else {
					String nombre = this.ventanaABMTerritorio.getTxtNombre().getText();
					this.tipoContacto.editarTipoContacto(this.ventanaABMTerritorio.getIdElementoAModificar(),nombre);
					this.refrescarTablaTipoDeContacto();
					this.tiposContactos = tipoContacto.obtenerTiposContacto();
					this.ventanaPersona.llenarTiposDeContacto(this.tiposContactos);
				}
			}else if(this.ventanaABMTerritorio.getTipoVentana()==3) {
				if(isNombreInvalido)this.ventanaABMTerritorio.advertirFaltaNombrePais();
				else {
					String nombre = this.ventanaABMTerritorio.getTxtNombre().getText();
					this.pais.agregarPais(nombre);
					this.refrescarTablaPais();
					this.paises = pais.obtenerPais();
					this.ventanaPersona.llenarPais(this.paises);
				}
			}else {
				if(isNombreInvalido)this.ventanaABMTerritorio.advertirFaltaNombrePais();
				else {
					String nombre = this.ventanaABMTerritorio.getTxtNombre().getText();
					this.pais.editarPais(this.ventanaABMTerritorio.getIdElementoAModificar(), nombre);
					this.refrescarTablaPais();
					this.paises = pais.obtenerPais();
					this.ventanaPersona.llenarPais(this.paises);
				}
			}
	//			this.refrescarTablaTipoDeContacto();
				if(!isNombreInvalido)this.ventanaABMTerritorio.cerrar();
		}
		
		private void editarTipoContacto(ActionEvent etc){
			int[] filasSeleccionadas = this.ventanaABMTipoContacto.getTablaPersonas().getSelectedRows();
			if(filasSeleccionadas.length == 1)
			{
				for (int fila : filasSeleccionadas)
				{
					this.ventanaABMTerritorio.setTipoVentana(2);
					this.ventanaABMTerritorio.setTitle("Editar Tipo de Contacto");
					this.ventanaABMTerritorio.setTxtNombre(this.tiposContactos.get(fila).getTipoContacto());
					this.ventanaABMTerritorio.setIdElementoAModificar(this.tiposContactos.get(fila).getIdTipoContacto());
					this.ventanaABMTerritorio.mostrarVentana();
				}
			}else this.ventanaABMTipoContacto.advertirSeleccionIncorrecta();
			this.refrescarTablaTipoDeContacto();
		}
		
		private void borrarTipoContacto(ActionEvent btc){
			int[] filasSeleccionadas = this.ventanaABMTipoContacto.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				if(!this.tipoContacto.borrarTipoContacto(this.tiposContactos.get(fila).getTipoContacto()))this.ventanaABMTipoContacto.advertenciaContactoUtilizandose();
				else{
					this.tiposContactos = tipoContacto.obtenerTiposContacto();
					this.ventanaPersona.llenarTiposDeContacto(this.tiposContactos);
				}
			}
			
			this.refrescarTablaTipoDeContacto();
		}
		
		private void mostrarABMPais(ActionEvent pp) {
			this.ventanaGeneralABMPais.mostrarVentana();
			refrescarTablaPais();
		}	

		private void mostrarABMProvincia(ActionEvent pprov) {
			this.paises = pais.obtenerPais();
			this.ventanaGeneralABMProvincia.llenarPais(this.paises);
			int pais;
			if(this.ventanaGeneralABMProvincia.getComboBoxListaPaises().getItemCount() == 0)pais = 0;
			else pais = ((PaisDTO)ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem()).getIdPais();
			
			//Pair<Integer, String> pais = new Pair<>(((ListaDesplegableDTO)ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem()).getIdlista(),((ListaDesplegableDTO)ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem()).getNombre());
			this.refrescarTablaProvincias(pais);
			this.ventanaGeneralABMProvincia.mostrarVentana();
		}
	
		
		
		
		
		private void agregarPais(ActionEvent agp) {
			this.ventanaABMTerritorio.setTipoVentana(3);
			this.ventanaABMTerritorio.setTitle("Agregar Pais");
			this.ventanaABMTerritorio.mostrarVentana();
		}
		
		private void editarPais(ActionEvent ep) {
			int[] filasSeleccionadas = this.ventanaGeneralABMPais.getTablaPersonas().getSelectedRows();
			if(filasSeleccionadas.length == 1)
			{
				for (int fila : filasSeleccionadas)
				{
					this.ventanaABMTerritorio.setTipoVentana(4);
					this.ventanaABMTerritorio.setTitle("Editar Pais");
					this.ventanaABMTerritorio.setTxtNombre(this.paises.get(fila).getPais());
					this.ventanaABMTerritorio.setIdElementoAModificar(this.paises.get(fila).getIdPais());
					this.ventanaABMTerritorio.mostrarVentana();
				}
			}else this.ventanaGeneralABMPais.advertirSeleccionIncorrecta();
			this.refrescarTablaPais();
		}
		
		private void borrarPais(ActionEvent bp) {
			int[] filasSeleccionadas = this.ventanaGeneralABMPais.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				if(!this.pais.borrarPais(this.paises.get(fila).getIdPais()))this.ventanaGeneralABMPais.advertenciaPaisUtilizandose();
				else{
					this.paises = pais.obtenerPais();
					this.ventanaPersona.llenarPais(this.paises);
				}
			}	
			this.refrescarTablaPais();
		}

		
		
		
		
		private void agregarProvincia(ActionEvent agprov) {
			this.ventanaCargarProvincia.setTipoVentana(1);
			this.ventanaCargarProvincia.setTitle("Agregar Provincia");
			this.ventanaCargarProvincia.getComboBoxPaises().setEnabled(true);
			this.paises = pais.obtenerPais();
			this.ventanaCargarProvincia.llenarPais(this.paises);
			this.ventanaCargarProvincia.getComboBoxPaises().setSelectedItem((PaisDTO)this.ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem());
			this.ventanaCargarProvincia.mostrarVentana();	
		}
		
		private void guardarProvincia(ActionEvent gp) {
			if(this.ventanaCargarProvincia.getComboBoxPaises().getItemCount() == 0) this.ventanaCargarProvincia.advertenciaPaisNoSeleccionado();
			else{		
				String regexp = "^$||\\s+$";
				if(Pattern.matches(regexp, this.ventanaCargarProvincia.getTxtNombreProvincia().getText()))this.ventanaCargarProvincia.advertenciaFaltaNombreProvincia();
				else{
					int pais = ((PaisDTO)this.ventanaCargarProvincia.getComboBoxPaises().getSelectedItem()).getIdPais();
					String nombre = this.ventanaCargarProvincia.getTxtNombreProvincia().getText();
					if(this.ventanaCargarProvincia.getTipoVentana()==1){
						if(!this.provincia.agregarProvincia(nombre, pais)){
							this.ventanaCargarProvincia.advertenciaProvinciaRepetida();
						}else{
							this.refrescarTablaProvincias(((PaisDTO)this.ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem()).getIdPais());
							if(this.ventanaPersona.getPais().getItemCount() == 0)this.provincias = provincia.obtenerProvincia(0);
							else this.provincias = provincia.obtenerProvincia(((PaisDTO)this.ventanaPersona.getPais().getSelectedItem()).getIdPais());
							this.ventanaPersona.llenarProvincias(this.provincias);
							if(this.ventanaPersona.getProvincia().getItemCount() == 0)this.localidades = localidad.obtenerLocalidad(0);
							else this.localidades = localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaPersona.getProvincia().getSelectedItem()).getIdProvincia());
							this.ventanaPersona.llenarLocalidades(this.localidades);
							this.ventanaCargarProvincia.cerrar();
						}
					}else{ 
						if(!this.provincia.editarProvincia(this.ventanaCargarProvincia.getIdProvinciaAModificar(), this.ventanaCargarProvincia.getTxtNombreProvincia().getText(), pais)){
							this.ventanaCargarProvincia.advertenciaProvinciaRepetida();
						}else{
							this.refrescarTablaProvincias(((PaisDTO)this.ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem()).getIdPais());
							if(this.ventanaPersona.getPais().getItemCount() == 0)this.provincias = provincia.obtenerProvincia(0);
							else this.provincias = provincia.obtenerProvincia(((PaisDTO)this.ventanaPersona.getPais().getSelectedItem()).getIdPais());
							this.ventanaPersona.llenarProvincias(this.provincias);
							if(this.ventanaPersona.getProvincia().getItemCount() == 0)this.localidades = localidad.obtenerLocalidad(0);
							else this.localidades = localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaPersona.getProvincia().getSelectedItem()).getIdProvincia());
							this.ventanaPersona.llenarLocalidades(this.localidades);
							this.ventanaCargarProvincia.cerrar();
						}
					}
				}
			}
		}
		
		private void editarProvincia(ActionEvent eprov) {
			int[] filasSeleccionadas = this.ventanaGeneralABMProvincia.getTablaProvincias().getSelectedRows();
			if(filasSeleccionadas.length == 1)//verificamos que seleccionara solo un valor para editar de la tabla provincias
			{
				for (int fila : filasSeleccionadas)
				{
					this.ventanaCargarProvincia.setTipoVentana(2);
					this.ventanaCargarProvincia.setTitle("Editar Provincia");
					this.paises = pais.obtenerPais();
					this.ventanaCargarProvincia.llenarPais(this.paises);
					this.ventanaCargarProvincia.setIdProvinciaAModificar(this.provincias.get(fila).getIdProvincia());
//					this.ventanaCargarProvincia.getComboBoxPaises().setSelectedItem(new ListaDesplegableDTO(this.ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem(), this.paises.get(fila).getNombre()));
					this.ventanaCargarProvincia.getComboBoxPaises().setSelectedItem((PaisDTO)this.ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem());
					this.ventanaCargarProvincia.getComboBoxPaises().setEnabled(false);
					this.ventanaCargarProvincia.setTxtNombreProvincia(this.provincias.get(fila).getProvincia());
					this.ventanaCargarProvincia.mostrarVentana();
				}
			}else this.ventanaGeneralABMProvincia.advertirSeleccionIncorrecta();
			this.refrescarTablaProvincias(((PaisDTO)this.ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem()).getIdPais());
			this.refrescarTabla();
		}
		
		private void borrarProvincia(ActionEvent bprov) {
			int[] filasSeleccionadas = this.ventanaGeneralABMProvincia.getTablaProvincias().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				if(!this.provincia.borrarProvincia(this.provincias.get(fila).getIdProvincia()))this.ventanaGeneralABMProvincia.advertenciaProvinciaUtilizandose();
				else {
					if(this.ventanaPersona.getPais().getItemCount() == 0)this.provincias = provincia.obtenerProvincia(0);
					else this.provincias = provincia.obtenerProvincia(((PaisDTO)this.ventanaPersona.getPais().getSelectedItem()).getIdPais());
					this.ventanaPersona.llenarProvincias(this.provincias);
					if(this.ventanaPersona.getProvincia().getItemCount() == 0)this.localidades = localidad.obtenerLocalidad(0);
					else this.localidades = localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaPersona.getProvincia().getSelectedItem()).getIdProvincia());
					this.ventanaPersona.llenarLocalidades(this.localidades);
				}
			}
			this.refrescarTablaProvincias(((PaisDTO)this.ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem()).getIdPais());
		}
		
		private void mostrarABMLocalidad(ActionEvent ploc) {
			this.paises = pais.obtenerPais();
			this.ventanaGeneralABMLocalidad.llenarPais(this.paises);
			if(this.ventanaGeneralABMLocalidad.getComboBoxListaPaises().getItemCount() == 0)this.provincias = provincia.obtenerProvincia(0);
			else this.provincias = provincia.obtenerProvincia(((PaisDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaPaises().getSelectedItem()).getIdPais());
			this.ventanaGeneralABMLocalidad.llenarProvincias(this.provincias);
			if(this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getItemCount() == 0)this.refrescarTablaLocalidades(0);
			else this.refrescarTablaLocalidades(((ProvinciaDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getSelectedItem()).getIdProvincia());
			this.ventanaGeneralABMLocalidad.mostrarVentana();
		
		}
		
		private void agregarLocalidad(ActionEvent agl) {
			this.ventanaCargarLocalidad.setTipoVentana(1);
			this.ventanaCargarLocalidad.setTitle("Agregar Localidad");
			this.ventanaCargarLocalidad.getComboBoxPaises().setEnabled(true);
			this.ventanaCargarLocalidad.getComboBoxProvincias().setEnabled(true);
			this.paises = pais.obtenerPais();
			this.ventanaCargarLocalidad.llenarPais(this.paises);
			this.ventanaCargarLocalidad.getComboBoxPaises().setSelectedItem((PaisDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaPaises().getSelectedItem());
			if(this.ventanaCargarLocalidad.getComboBoxPaises().getItemCount() == 0)this.provincias = provincia.obtenerProvincia(0);
			else this.provincias = provincia.obtenerProvincia(((PaisDTO)this.ventanaCargarLocalidad.getComboBoxPaises().getSelectedItem()).getIdPais());
			this.ventanaCargarLocalidad.llenarProvincias(this.provincias);
			this.ventanaCargarLocalidad.getComboBoxProvincias().setSelectedItem((ProvinciaDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getSelectedItem());
			this.ventanaCargarLocalidad.mostrarVentana();	
			
		}
		
		private void guardarLocalidad(ActionEvent gl) {
			if((this.ventanaCargarLocalidad.getComboBoxProvincias().getItemCount() == 0)) this.ventanaCargarLocalidad.advertenciaProvinciaNoSeleccionada();
			else { 
				String regexp = "^$||\\s+$";
				if(Pattern.matches(regexp, this.ventanaCargarLocalidad.getTxtNombreLocalidad().getText()))this.ventanaCargarLocalidad.advertenciaFaltaNombreLocalidad();
				else{
					int provincia = ((ProvinciaDTO)this.ventanaCargarLocalidad.getComboBoxProvincias().getSelectedItem()).getIdProvincia();
					String nombre = this.ventanaCargarLocalidad.getTxtNombreLocalidad().getText(); 
					if(this.ventanaCargarLocalidad.getTipoVentana()==1){
						if(!this.localidad.agregarLocalidad(nombre, provincia)){
							this.ventanaCargarLocalidad.advertenciaLocalidadRepetida();
						}else{
							if(this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getItemCount() == 0)this.refrescarTablaLocalidades(0);
							else{
								this.refrescarTablaLocalidades(((ProvinciaDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getSelectedItem()).getIdProvincia());
								if(this.ventanaPersona.getProvincia().getItemCount() == 0)this.localidades = localidad.obtenerLocalidad(0);
								else this.localidades = localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaPersona.getProvincia().getSelectedItem()).getIdProvincia());
								this.ventanaPersona.llenarLocalidades(this.localidades);
							}
							this.ventanaCargarLocalidad.cerrar();
						}
					}else{ 
						if(!this.localidad.editarLocalidad(this.ventanaCargarLocalidad.getIdLocalidadAModificar(), this.ventanaCargarLocalidad.getTxtNombreLocalidad().getText(), provincia)){
							this.ventanaCargarLocalidad.advertenciaLocalidadRepetida();
						}else{
							if(this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getItemCount() == 0)this.refrescarTablaLocalidades(0);
							else {
								this.refrescarTablaLocalidades(((ProvinciaDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getSelectedItem()).getIdProvincia());
								if(this.ventanaPersona.getProvincia().getItemCount() == 0)this.localidades = localidad.obtenerLocalidad(0);
								else this.localidades = localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaPersona.getProvincia().getSelectedItem()).getIdProvincia());
								this.ventanaPersona.llenarLocalidades(this.localidades);
							}
							this.ventanaCargarLocalidad.cerrar();
						}
					}
				}
			}
		}
		
		private void editarLocalidad(ActionEvent el) {
			int[] filasSeleccionadas = this.ventanaGeneralABMLocalidad.getTablaProvincias().getSelectedRows();
			if(filasSeleccionadas.length == 1)//verificamos que seleccionara solo un valor para editar de la tabla localidades
			{
				for (int fila : filasSeleccionadas)
				{
					this.ventanaCargarLocalidad.setTipoVentana(2);
					this.ventanaCargarLocalidad.setTitle("Editar Localidad");
					this.paises = pais.obtenerPais();
					this.ventanaCargarLocalidad.llenarPais(this.paises);
					this.provincias = provincia.obtenerProvincia(((PaisDTO)this.ventanaCargarLocalidad.getComboBoxPaises().getSelectedItem()).getIdPais());
					this.ventanaCargarLocalidad.llenarProvincias(this.provincias);
					this.ventanaCargarLocalidad.setIdLocalidadAModificar(this.localidades.get(fila).getIdLocalidad());
//					this.ventanaCargarProvincia.getComboBoxPaises().setSelectedItem(new ListaDesplegableDTO(this.ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem(), this.paises.get(fila).getNombre()));
					this.ventanaCargarLocalidad.getComboBoxPaises().setSelectedItem((PaisDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaPaises().getSelectedItem());
					this.ventanaCargarLocalidad.getComboBoxPaises().setEnabled(false);
					this.ventanaCargarLocalidad.getComboBoxProvincias().setSelectedItem((ProvinciaDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getSelectedItem());
					this.ventanaCargarLocalidad.getComboBoxProvincias().setEnabled(false);
					this.ventanaCargarLocalidad.setTxtNombreLocalidad(this.localidades.get(fila).getLocalidad());
					this.ventanaCargarLocalidad.mostrarVentana();
				}
			}else this.ventanaGeneralABMLocalidad.advertirSeleccionIncorrecta();
			if(this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getItemCount() == 0)this.refrescarTablaLocalidades(0);
			else this.refrescarTablaLocalidades(((ProvinciaDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getSelectedItem()).getIdProvincia());
			this.refrescarTabla();
			
		}
		
		private void borrarLocalidad(ActionEvent bl) {
			int[] filasSeleccionadas = this.ventanaGeneralABMLocalidad.getTablaProvincias().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				if(!this.localidad.borrarLocalidad(this.localidades.get(fila).getIdLocalidad()))this.ventanaGeneralABMLocalidad.advertenciaLocalidadUtilizandose();
				else {
					if(this.ventanaPersona.getProvincia().getItemCount() == 0)this.localidades = localidad.obtenerLocalidad(0);
					else this.localidades = localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaPersona.getProvincia().getSelectedItem()).getIdProvincia());
					this.ventanaPersona.llenarLocalidades(this.localidades);
				}
			}
			this.refrescarTablaLocalidades(((ProvinciaDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getSelectedItem()).getIdProvincia());			
		}
		
		//metodo que carga las listas desplegables del jframe ventanaPersona
		private void cargarListasDesplegables(){
			this.tiposContactos = tipoContacto.obtenerTiposContacto();
			this.ventanaPersona.llenarTiposDeContacto(this.tiposContactos);
			this.paises = pais.obtenerPais();
			this.ventanaPersona.llenarPais(this.paises);
			if(this.ventanaPersona.getPais().getItemCount() == 0)this.provincias = provincia.obtenerProvincia(0);
			else this.provincias = provincia.obtenerProvincia(((PaisDTO)this.ventanaPersona.getPais().getSelectedItem()).getIdPais());
			this.ventanaPersona.llenarProvincias(this.provincias);
			if(this.ventanaPersona.getProvincia().getItemCount() == 0)this.localidades = localidad.obtenerLocalidad(0);
			else this.localidades = localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaPersona.getProvincia().getSelectedItem()).getIdProvincia());
			this.ventanaPersona.llenarLocalidades(this.localidades);
		}

		//metodo que actualiza la lista deplegable de las provincias del jframe ventanaPersona
		private void actualizarProvincia(ItemEvent ps)
		{
			if(this.ventanaPersona.getPais().isLoaded()){
				this.provincias = provincia.obtenerProvincia(((PaisDTO)this.ventanaPersona.getPais().getSelectedItem()).getIdPais());
				this.ventanaPersona.llenarProvincias(this.provincias);
				if(this.ventanaPersona.getProvincia().isLoaded()){
					if(this.ventanaPersona.getProvincia().getItemCount() == 0)this.localidades = localidad.obtenerLocalidad(0);
					else this.localidades = localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaPersona.getProvincia().getSelectedItem()).getIdProvincia());
					this.ventanaPersona.llenarLocalidades(this.localidades);
				}
			}
		}
		
		//metodo que actualiza la lista desplegable de las localidades del jframe ventanaPersona
		private void actualizarLocalidad(ItemEvent pr)
		{
			if(this.ventanaPersona.getProvincia().isLoaded()){
				this.localidades =  localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaPersona.getProvincia().getSelectedItem()).getIdProvincia());
				this.ventanaPersona.llenarLocalidades(this.localidades);
			}
		}
		
		//metodo que actualiza la tabla de las localidades del jframe ventanaGeneralABMProvincia
		private void actualizarTblProvincia(ItemEvent acprov) {
			if(this.ventanaGeneralABMProvincia.getComboBoxListaPaises().isLoaded()){
				this.provincias =  provincia.obtenerProvincia(((PaisDTO)this.ventanaGeneralABMProvincia.getComboBoxListaPaises().getSelectedItem()).getIdPais());
				this.ventanaGeneralABMProvincia.llenarTabla(this.provincias);
			}	
			
		}
		
		//metodo que actualiza la lista desplegable de las provincias del jframe ventanaGeneralABMLocalidad
		private void actualizarCmbProvincia(ItemEvent acps) {
			if(this.ventanaGeneralABMLocalidad.getComboBoxListaPaises().isLoaded()){
				this.provincias = provincia.obtenerProvincia(((PaisDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaPaises().getSelectedItem()).getIdPais());
				this.ventanaGeneralABMLocalidad.llenarProvincias(this.provincias);
				if(this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().isLoaded()){
					if(this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getItemCount() == 0)this.localidades =  localidad.obtenerLocalidad(0);				
					else this.localidades =  localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getSelectedItem()).getIdProvincia());
					this.ventanaGeneralABMLocalidad.llenarTabla(this.localidades);
				}	
			}		
		}
		
		//metodo que actualiza la tabla de las localidades del jframe ventanaGeneralABMLocalidad
		private void actualizarTblLocalidades(ItemEvent acprov) {
			if(this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().isLoaded()){
				this.localidades =  localidad.obtenerLocalidad(((ProvinciaDTO)this.ventanaGeneralABMLocalidad.getComboBoxListaProvincias().getSelectedItem()).getIdProvincia());
				this.ventanaGeneralABMLocalidad.llenarTabla(this.localidades);
			}	
		}
		
		private void actualizarCmbProvinciaEnCargarLocalidad(ItemEvent acpsl){
			if(this.ventanaCargarLocalidad.getComboBoxPaises().isLoaded()){
				this.provincias = provincia.obtenerProvincia(((PaisDTO)this.ventanaCargarLocalidad.getComboBoxPaises().getSelectedItem()).getIdPais());
				this.ventanaCargarLocalidad.llenarProvincias(this.provincias);
			}
		}
		
		private void revisarEscrituraTelefono(KeyEvent ctel){
			if(!Character.isDigit(ctel.getKeyChar()) && ctel.getKeyChar() != KeyEvent.VK_BACK_SPACE){
				ctel.consume();
				this.ventanaPersona.advertirEscrituraTelefono();
            }
		}
		
		private void revisarEscrituraAltura(KeyEvent calt){
			if(!Character.isDigit(calt.getKeyChar()) && calt.getKeyChar() != KeyEvent.VK_BACK_SPACE){
				calt.consume();
				this.ventanaPersona.advertirEscrituraAltura();
            }
		}
		
		private void revisarEscrituraPiso(KeyEvent cpiso){
			if(!Character.isDigit(cpiso.getKeyChar()) && cpiso.getKeyChar() != KeyEvent.VK_BACK_SPACE){
				cpiso.consume();
				this.ventanaPersona.advertirEscrituraPiso();
            }
		}
		
		private void revisarEscrituraCiudadPreferida(KeyEvent cciup){
			String ciudadPreferida;
			ciudadPreferida = this.ventanaPersona.getTxtCiudadPreferida().getText().toUpperCase();
			this.ventanaPersona.setTxtCiudadPreferida(ciudadPreferida);    
		}

		//metodo que guarda un persona nueva en la agenda o guarda los cambios de una persona existente en la agenda
		private void guardarPersona(ActionEvent p) {
			String regexp = "^$||\\s+$";
			if(Pattern.matches(regexp, this.ventanaPersona.getTxtNombre().getText()))ventanaPersona.advertirFaltaNombreContacto();
			else{
				String nombre = this.ventanaPersona.getTxtNombre().getText();
				String tel = ventanaPersona.getTxtTelefono().getText();
				String mail = ventanaPersona.getTxtMail().getText();
				
				if(tel.isEmpty()&&mail.isEmpty()){ //verifica que no se guarde un contacto sin un medio de contacto(mail o telefono)
					ventanaPersona.advertirFaltaMedioDeContacto();
				}else{
					String fechaNac = castearFecha(); //hacer test
					if(ventanaPersona.getTipoContacto().getItemCount() == 0 || ventanaPersona.getPais().getItemCount() == 0 || ventanaPersona.getProvincia().getItemCount() == 0 || ventanaPersona.getLocalidad().getItemCount() == 0)ventanaPersona.advertirFaltaItemListaDesplegable();
					else{
						Pair<Integer, String> tipoContacto = new Pair<>(((TipoContactoDTO)ventanaPersona.getTipoContacto().getSelectedItem()).getIdTipoContacto(),((TipoContactoDTO)ventanaPersona.getTipoContacto().getSelectedItem()).getTipoContacto());
						Pair<Integer, String> pais = new Pair<>(((PaisDTO)ventanaPersona.getPais().getSelectedItem()).getIdPais(),((PaisDTO)ventanaPersona.getPais().getSelectedItem()).getPais());//new Pair<>(1,"Argentina");			
						Pair<Integer, String> provincia = new Pair<>(((ProvinciaDTO)ventanaPersona.getProvincia().getSelectedItem()).getIdProvincia(),((ProvinciaDTO)ventanaPersona.getProvincia().getSelectedItem()).getProvincia());
						Pair<Integer, String> localidad = new Pair<>(((LocalidadDTO)ventanaPersona.getLocalidad().getSelectedItem()).getIdLocalidad(),((LocalidadDTO)ventanaPersona.getLocalidad().getSelectedItem()).getLocalidad());
						String calle = ventanaPersona.getTxtCalle().getText();
						String altura = ventanaPersona.getTxtAltura().getText();
						String piso = ventanaPersona.getTxtPiso().getText();
						String departamento = ventanaPersona.getTxtDepartamento().getText();
						DomicilioDTO nuevoDomicilio = new DomicilioDTO(0, pais, provincia, localidad, calle, altura, piso, departamento);
						if(Pattern.matches(regexp, this.ventanaPersona.getTxtCiudadPreferida().getText()))ventanaPersona.advertirFaltaCiudadPreferida();
						else{
							CiudadPreferidaDTO ciudadPreferida = new CiudadPreferidaDTO(0,this.ventanaPersona.getTxtCiudadPreferida().getText().toUpperCase());
							PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel, mail, fechaNac, tipoContacto, nuevoDomicilio, ciudadPreferida);
							if(this.ventanaPersona.getTipoVentana()==1){
								if(!this.agenda.agregarPersona(nuevaPersona)){
									this.ventanaPersona.advertenciaPersonaRepetida();
								}else{
									this.refrescarTabla();
									this.ventanaPersona.cerrar();
								}
							}else{ 
	//							nuevaPersona.setIdPersona(Integer.parseInt(ventanaPersona.getTxtNumContacto().getText()));
								nuevaPersona.setIdPersona(ventanaPersona.getIdPersona());
								if(!this.agenda.editarPersona(nuevaPersona)){
									this.ventanaPersona.advertenciaPersonaRepetida();
								}else{
									this.refrescarTabla();
									this.ventanaPersona.cerrar();
								}
							}
						}
					}
				}
			}
		}

		//evento muestra reporte agenda
		private void mostrarReporte(ActionEvent r) {
			this.ventanaReporte.mostrarVentana();
		}
		
		private void generarReportes(ActionEvent gr) {
			if(this.ventanaReporte.getRdbtnReporteAgenda().isSelected()){
				ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas());
				reporte.mostrar();
			}
			if(this.ventanaReporte.getRdbtnReporteGroupCP().isSelected()){
				ReporteAgendaGCP reporteGCP = new ReporteAgendaGCP();
				reporteGCP.mostrar();	
			}
			if(this.ventanaReporte.getRdbtnReporteGroupCPDM().isSelected()){
				ReporteAgendaGCPMD reporteGCPMD = new ReporteAgendaGCPMD();
				reporteGCPMD.mostrar();
			}
			if(this.ventanaReporte.getRdbtnReporteGroupCPPaPrL().isSelected()){
				ReporteAgendaGCPaPrL reporteGCPaPrL = new ReporteAgendaGCPaPrL();
				reporteGCPaPrL.mostrar();
			}
			this.ventanaReporte.cerrar();
		}
		
		private void cancelarReportes(ActionEvent cr) {
			this.ventanaReporte.cerrar();
		}

		//evento que elimina una persona de la agenda
		public void borrarPersona(ActionEvent s)
		{
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarPersona(this.personasEnTabla.get(fila));
			}
			
			this.refrescarTabla();
		}
		
		public void inicializar()
		{
			this.refrescarTabla();
			this.vista.show();
		}
		
		/////
		//metodos actualizadores de jtables
		/////
		
		//refresca tabla de agenda
		private void refrescarTabla()
		{
			this.personasEnTabla = agenda.obtenerPersonas();
			this.vista.llenarTabla(this.personasEnTabla);
		}
		
		//refresca tabla de ABM tipoContacto
		private void refrescarTablaTipoDeContacto()
		{
			this.tiposContactos = tipoContacto.obtenerTiposContacto();
			this.ventanaABMTipoContacto.llenarTabla(this.tiposContactos);
		}

		private void refrescarTablaPais() {
			this.paises = pais.obtenerPais();
			this.ventanaGeneralABMPais.llenarTabla(this.paises);	
		}
		
		//refresca tabla de ABM provincia
		private void refrescarTablaProvincias(int Pais)
		{
			this.provincias = provincia.obtenerProvincia(Pais);
			this.ventanaGeneralABMProvincia.llenarTabla(this.provincias);
		}
		
		//refresca tabla de ABM localidad
		private void refrescarTablaLocalidades(int Provincia) {
			this.localidades = localidad.obtenerLocalidad(Provincia);
			this.ventanaGeneralABMLocalidad.llenarTabla(this.localidades);
			
		}
		

		
		//metodo para formatear la fecha de JDataChooser a formato String yyyy-mm-dd
		public String castearFecha() {
			try{
				String dia = ventanaPersona.getFechaNac().getDate().toString();
	            int espacios = 0;
	            int cont = 0;
	            StringBuilder sb = new StringBuilder();
	            String Anio = "";
	            String Mes = "-"+(ventanaPersona.getFechaNac().getDate().getMonth()+1);
	            for(int i=0;i<dia.length();i++){
	                if(dia.charAt(i)==' '){
	                    espacios++;
	                }else if(espacios==2){
	                    if(cont==0){
	                        sb.append("-"+dia.charAt(i));
	                        cont++;
	                    }else{
	                        sb.append(dia.charAt(i));
	                    }
	                }else if(espacios==5){
	                        Anio = Anio+dia.charAt(i);
	                }
	            }
	            sb.insert(0, Anio);
	            sb.insert(Anio.length(), Mes);
	            return sb.toString();
			}catch(NullPointerException ex){
				return "";
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
			return null;
		}
		
		
//		@Override
//		public void actionPerformed(ActionEvent e) { }
		
		
		
}
