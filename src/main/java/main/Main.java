package main;


import javax.swing.UIManager;

import modelo.Agenda;
import modelo.Localidad;
import modelo.Pais;
import modelo.Provincia;
import modelo.TipoContacto;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.Controlador;
import presentacion.vista.Vista;


public class Main 
{

	public static void main(String[] args) 
	{
		try{
			UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
			Vista vista = new Vista();
			Agenda modelo = new Agenda(new DAOSQLFactory());
			TipoContacto tipoContacto = new TipoContacto(new DAOSQLFactory());
			Pais pais = new Pais(new DAOSQLFactory());
			Provincia provincia = new Provincia(new DAOSQLFactory());
			Localidad localidad = new Localidad(new DAOSQLFactory());
			Controlador controlador = new Controlador(vista, modelo, tipoContacto, pais, provincia, localidad);
			controlador.inicializar();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
