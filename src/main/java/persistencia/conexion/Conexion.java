package persistencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import presentacion.vista.Vista;





public class Conexion 
{
	public static Conexion instancia;
	private Connection connection;
	private Logger log = Logger.getLogger(Conexion.class);	
	
	private Conexion()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver"); // quitar si no es necesario
			this.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/agenda","root","");
			this.connection.setAutoCommit(false);
			log.info("Conexi�n exitosa");
		}
		catch(Exception e)
		{
			log.error("Conexi�n fallida", e);
			Vista.advertirFallaConexion();
		}
	}
	
	
	public static Conexion getConexion()   
	{								
		if(instancia == null)
		{
			instancia = new Conexion();
		}
		return instancia;
	}

	public Connection getSQLConexion() 
	{
		return this.connection;
	}
	
	public void cerrarConexion()
	{
		try 
		{
			this.connection.close();
			log.info("Conexi�n cerrada");
		}
		catch (SQLException e) 
		{
			log.error("Error al cerrar la conexi�n!", e);
		}
		instancia = null;
	}
}
