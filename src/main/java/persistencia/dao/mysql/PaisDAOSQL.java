package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.PaisDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PaisDAO;

public class PaisDAOSQL implements PaisDAO{

	private static final String insertPais = "INSERT INTO pais(idpais, pais) VALUES(?, ?)";
	private static final String delete = "DELETE FROM pais WHERE idpais = ?";
	private static final String editarPais = "UPDATE pais SET pais = ? WHERE idpais = ?";
	private static final String readallPais = "SELECT * FROM pais";	
	
	public boolean insert(String pais) {
		PreparedStatement statementInsertarPais;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		
		try
		{
			statementInsertarPais = conexion.prepareStatement(insertPais);
			statementInsertarPais.setInt(1,0);
			statementInsertarPais.setString(2, pais);
			if(statementInsertarPais.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
			else conexion.rollback();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	
	public boolean delete(int pais_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, pais_a_eliminar);
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	
	public boolean update(int idPaisAModificar, String paisModificado) {
		PreparedStatement statementEditarPais;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditarExitoso = false;
		
		try
		{
			statementEditarPais = conexion.prepareStatement(editarPais);
			statementEditarPais.setString(1,paisModificado);
			statementEditarPais.setInt(2, idPaisAModificar);
			if(statementEditarPais.executeUpdate() > 0)
			{
				conexion.commit();
				isEditarExitoso = true;
			}
			else conexion.rollback();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isEditarExitoso;
	}
	
	public List<PaisDTO> readAllPais()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PaisDTO> pais = new ArrayList<PaisDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readallPais);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				pais.add(getPaisDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return pais;
	}
	
	private PaisDTO getPaisDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idpais");
		String nombre = resultSet.getString("pais");

		return new PaisDTO(id, nombre);
	}
	
}
