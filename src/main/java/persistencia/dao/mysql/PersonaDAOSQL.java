package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.CiudadPreferidaDTO;
import dto.DomicilioDTO;
import dto.PersonaDTO;
import javafx.util.Pair;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insertPersona = "INSERT INTO personas(idPersona, nombre, fechaNac, telefono, mail, tipo_contacto, domicilio, ciudadPreferida) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String insertDomicilio = "INSERT INTO domicilio(iddomicilio, calle, altura, piso, departamento, idlocalidad) VALUES (?, ?, ?, ?, ?, ?)";
	private static final String insertCiudadPreferida = "INSERT INTO ciudadpreferida(idciudadPreferida, ciudadPreferida) VALUES (?, ?)";
	private static final String readlastIdDomicilio = "SELECT MAX(iddomicilio) as iddomicilio FROM domicilio";
	private static final String readlastIdCiudadPreferida = "SELECT MAX(idciudadPreferida) as idciudadPreferida FROM ciudadpreferida";
	private static final String readDomicilioIsExist = "SELECT iddomicilio, calle, altura, piso, departamento, idlocalidad From domicilio WHERE calle = ? and altura = ? and piso = ? and departamento = ? and idlocalidad = ?";
	private static final String readCiudadFavoritaIsExist = "SELECT idciudadPreferida, ciudadPreferida FROM ciudadpreferida WHERE ciudadPreferida = ?";
	//	private static final String readLocalidadxProvinciaxPais = "SELECT idlocalidadxprovinciaxpais FROM localidadxprovinciaxpais WHERE idprovincia = ? and idlocalidad = ? and idpais = ? ";
	private static final String readPersonaIsExist = "SELECT COUNT(idpersona) as cantidad FROM personas WHERE nombre = ?";
	private static final String readPersonaIsExistOther = "SELECT COUNT(idpersona) as cantidad FROM personas WHERE nombre = ? and idpersona != ?";
	private static final String updatePersona = "UPDATE personas SET nombre = ?, fechaNac = ?, telefono = ?, mail = ?, tipo_contacto = ?, domicilio  = ?, ciudadPreferida = ? WHERE idPersona = ?";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	//private static final String readall = "SELECT * FROM personas";
//	private static final String readall = "SELECT idpersona, nombre, fechaNac, telefono, mail, tipocontacto.tipoContacto, provincia.provincia, localidad.localidad, calle, altura, piso, departamento FROM personas join tipocontacto on personas.tipo_contacto = tipocontacto.idtipoContacto join domicilio on personas.domicilio = domicilio.iddomicilio join localidadxprovincia on domicilio.idlocalidadxprovincia = localidadxprovincia.idlocalidadxprovincia join provincia on localidadxprovincia.idprovincia = provincia.idprovincia join localidad on localidadxprovincia.idlocalidad = localidad.idlocalidad";

	//	private static final String readall = "SELECT idpersona, nombre, fechaNac, telefono, mail, tipocontacto.idtipoContacto, tipocontacto.tipoContacto, domicilio.iddomicilio, pais.idpais, pais.pais, provincia.idprovincia, provincia.provincia, localidad.idlocalidad, localidad.localidad, calle, altura, piso, departamento FROM personas join tipocontacto on personas.tipo_contacto = tipocontacto.idtipoContacto join domicilio on personas.domicilio = domicilio.iddomicilio join localidadxprovinciaxpais on domicilio.idlocalidadxprovinciaxpais = localidadxprovinciaxpais.idlocalidadxprovinciaxpais join pais on localidadxprovinciaxpais.idpais = pais.idpais join provincia on localidadxprovinciaxpais.idprovincia = provincia.idprovincia join localidad on localidadxprovinciaxpais.idlocalidad = localidad.idlocalidad";
	private static final String readall = "SELECT idpersona, nombre, fechaNac, telefono, mail, tipocontacto.idtipoContacto, tipocontacto.tipoContacto, domicilio.iddomicilio, pais.idpais, pais.pais, provincia.idprovincia, provincia.provincia, localidad.idlocalidad, localidad.localidad, calle, altura, piso, departamento, ciudadpreferida.idciudadPreferida, ciudadpreferida.ciudadPreferida FROM personas join tipocontacto on personas.tipo_contacto = tipocontacto.idtipoContacto join domicilio on personas.domicilio = domicilio.iddomicilio join localidad on localidad.idlocalidad = domicilio.idlocalidad join provincia on provincia.idprovincia = localidad.idprovincia join pais on pais.idpais = provincia.idpais join ciudadpreferida on personas.ciudadPreferida = ciudadPreferida.idciudadPreferida";
	
	
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statementReadCiudadFavoritaIsExist;
		PreparedStatement statementReadDomicilioIsExist;
		PreparedStatement statementDomicilio;
		PreparedStatement statementCiudadPreferida;
		PreparedStatement statementPersona;
		PreparedStatement statementLastIdDomicilio;
		PreparedStatement statementLastIdCiudadPreferida;
		PreparedStatement statementIsExistPersona;
		ResultSet resultSetReadCiudadFavoritaIsExist;
		ResultSet resultSetReadDomicilioIsExist;
		ResultSet resultSetLastIdDomicilio;
		ResultSet resultSetLastIdCiudadPreferida;
		ResultSet resultSetIsExistPersona;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statementReadCiudadFavoritaIsExist = conexion.prepareStatement(readCiudadFavoritaIsExist);
			statementReadCiudadFavoritaIsExist.setString(1, persona.getCiudadPreferida().getCiudadPreferida());
			
			resultSetReadCiudadFavoritaIsExist = statementReadCiudadFavoritaIsExist.executeQuery();
			
			if(resultSetReadCiudadFavoritaIsExist.next()){
			
				statementReadDomicilioIsExist = conexion.prepareStatement(readDomicilioIsExist);
				statementReadDomicilioIsExist.setString(1, persona.getDomicilio().getCalle());
				statementReadDomicilioIsExist.setString(2, persona.getDomicilio().getAltura());
				statementReadDomicilioIsExist.setString(3, persona.getDomicilio().getPiso());
				statementReadDomicilioIsExist.setString(4, persona.getDomicilio().getDepartamento());
				statementReadDomicilioIsExist.setInt(5, persona.getDomicilio().getLocalidad().getKey());
				
				resultSetReadDomicilioIsExist = statementReadDomicilioIsExist.executeQuery();
				
				if(resultSetReadDomicilioIsExist.next()){
					
					statementIsExistPersona = conexion.prepareStatement(readPersonaIsExist);
					statementIsExistPersona.setString(1, persona.getNombre());
					resultSetIsExistPersona = statementIsExistPersona.executeQuery();
					
					if(resultSetIsExistPersona.next()){
						if(!(resultSetIsExistPersona.getInt("cantidad")>=1)){
							statementPersona = conexion.prepareStatement(insertPersona);
							statementPersona.setInt(1, persona.getIdPersona());
							statementPersona.setString(2, persona.getNombre());
							statementPersona.setString(3, persona.getFechaNac());
							statementPersona.setString(4, persona.getTelefono());
							statementPersona.setString(5, persona.getMail());
							statementPersona.setInt(6, persona.getTipoContacto().getKey());
							statementPersona.setInt(7, resultSetReadDomicilioIsExist.getInt("iddomicilio"));
							statementPersona.setInt(8, resultSetReadCiudadFavoritaIsExist.getInt("idciudadPreferida"));
							if(statementPersona.executeUpdate() > 0)
							{
								conexion.commit();
								isInsertExitoso = true;
							}
							else conexion.rollback();
						}
					}
				}else{
					
					statementDomicilio = conexion.prepareStatement(insertDomicilio);
					statementDomicilio.setInt(1, 0);
					statementDomicilio.setString(2, persona.getDomicilio().getCalle());
					statementDomicilio.setString(3, persona.getDomicilio().getAltura());
					statementDomicilio.setString(4, persona.getDomicilio().getPiso());
					statementDomicilio.setString(5, persona.getDomicilio().getDepartamento());
					statementDomicilio.setInt(6, persona.getDomicilio().getLocalidad().getKey());
					if(statementDomicilio.executeUpdate() > 0)
					{
						
						statementIsExistPersona = conexion.prepareStatement(readPersonaIsExist);
						statementIsExistPersona.setString(1, persona.getNombre());
						resultSetIsExistPersona = statementIsExistPersona.executeQuery();
						
						if(resultSetIsExistPersona.next()){
							if(!(resultSetIsExistPersona.getInt("cantidad")>=1)){
								statementPersona = conexion.prepareStatement(insertPersona);
								statementPersona.setInt(1, persona.getIdPersona());
								statementPersona.setString(2, persona.getNombre());
								statementPersona.setString(3, persona.getFechaNac());
								statementPersona.setString(4, persona.getTelefono());
								statementPersona.setString(5, persona.getMail());
								statementPersona.setInt(6, persona.getTipoContacto().getKey());
								
								statementLastIdDomicilio = conexion.prepareStatement(readlastIdDomicilio);
								resultSetLastIdDomicilio = statementLastIdDomicilio.executeQuery();
								if(resultSetLastIdDomicilio.next())
								statementPersona.setInt(7, resultSetLastIdDomicilio.getInt("iddomicilio"));
								
								statementPersona.setInt(8, resultSetReadCiudadFavoritaIsExist.getInt("idciudadPreferida"));
								if(statementPersona.executeUpdate() > 0)
								{
									conexion.commit();
									isInsertExitoso = true;
								}
								else conexion.rollback();
							}else conexion.rollback();
						}else conexion.rollback();
					}
					else conexion.rollback();
				}
			}else{ 
				statementCiudadPreferida = conexion.prepareStatement(insertCiudadPreferida);
				statementCiudadPreferida.setInt(1, 0);
				statementCiudadPreferida.setString(2, persona.getCiudadPreferida().getCiudadPreferida());
				
				if(statementCiudadPreferida.executeUpdate() > 0)
				{
					statementReadDomicilioIsExist = conexion.prepareStatement(readDomicilioIsExist);
					statementReadDomicilioIsExist.setString(1, persona.getDomicilio().getCalle());
					statementReadDomicilioIsExist.setString(2, persona.getDomicilio().getAltura());
					statementReadDomicilioIsExist.setString(3, persona.getDomicilio().getPiso());
					statementReadDomicilioIsExist.setString(4, persona.getDomicilio().getDepartamento());
					statementReadDomicilioIsExist.setInt(5, persona.getDomicilio().getLocalidad().getKey());
					
					resultSetReadDomicilioIsExist = statementReadDomicilioIsExist.executeQuery();
					
					if(resultSetReadDomicilioIsExist.next()){
						
						statementIsExistPersona = conexion.prepareStatement(readPersonaIsExist);
						statementIsExistPersona.setString(1, persona.getNombre());
						resultSetIsExistPersona = statementIsExistPersona.executeQuery();
						
						if(resultSetIsExistPersona.next()){
							if(!(resultSetIsExistPersona.getInt("cantidad")>=1)){
								statementPersona = conexion.prepareStatement(insertPersona);
								statementPersona.setInt(1, persona.getIdPersona());
								statementPersona.setString(2, persona.getNombre());
								statementPersona.setString(3, persona.getFechaNac());
								statementPersona.setString(4, persona.getTelefono());
								statementPersona.setString(5, persona.getMail());
								statementPersona.setInt(6, persona.getTipoContacto().getKey());
								statementPersona.setInt(7, resultSetReadDomicilioIsExist.getInt("iddomicilio"));
								
								statementLastIdCiudadPreferida = conexion.prepareStatement(readlastIdCiudadPreferida);
								resultSetLastIdCiudadPreferida = statementLastIdCiudadPreferida.executeQuery();
								if(resultSetLastIdCiudadPreferida.next())
									statementPersona.setInt(8, resultSetLastIdCiudadPreferida.getInt("idciudadPreferida"));
								
								if(statementPersona.executeUpdate() > 0)
								{
									conexion.commit();
									isInsertExitoso = true;
								}
								else conexion.rollback();
							}
						}
					}else{
						
						statementDomicilio = conexion.prepareStatement(insertDomicilio);
						statementDomicilio.setInt(1, 0);
						statementDomicilio.setString(2, persona.getDomicilio().getCalle());
						statementDomicilio.setString(3, persona.getDomicilio().getAltura());
						statementDomicilio.setString(4, persona.getDomicilio().getPiso());
						statementDomicilio.setString(5, persona.getDomicilio().getDepartamento());
						statementDomicilio.setInt(6, persona.getDomicilio().getLocalidad().getKey());
						if(statementDomicilio.executeUpdate() > 0)
						{
							
							statementIsExistPersona = conexion.prepareStatement(readPersonaIsExist);
							statementIsExistPersona.setString(1, persona.getNombre());
							resultSetIsExistPersona = statementIsExistPersona.executeQuery();
							
							if(resultSetIsExistPersona.next()){
								if(!(resultSetIsExistPersona.getInt("cantidad")>=1)){
									statementPersona = conexion.prepareStatement(insertPersona);
									statementPersona.setInt(1, persona.getIdPersona());
									statementPersona.setString(2, persona.getNombre());
									statementPersona.setString(3, persona.getFechaNac());
									statementPersona.setString(4, persona.getTelefono());
									statementPersona.setString(5, persona.getMail());
									statementPersona.setInt(6, persona.getTipoContacto().getKey());
									
									statementLastIdDomicilio = conexion.prepareStatement(readlastIdDomicilio);
									resultSetLastIdDomicilio = statementLastIdDomicilio.executeQuery();
									if(resultSetLastIdDomicilio.next())
									statementPersona.setInt(7, resultSetLastIdDomicilio.getInt("iddomicilio"));
									
									statementLastIdCiudadPreferida = conexion.prepareStatement(readlastIdCiudadPreferida);
									resultSetLastIdCiudadPreferida = statementLastIdCiudadPreferida.executeQuery();
									if(resultSetLastIdCiudadPreferida.next())
										statementPersona.setInt(8, resultSetLastIdCiudadPreferida.getInt("idciudadPreferida"));
									
									if(statementPersona.executeUpdate() > 0)
									{
										conexion.commit();
										isInsertExitoso = true;
									}
									else conexion.rollback();
								}else conexion.rollback();
							}else conexion.rollback();
						}
						else conexion.rollback();
					}
				}
				else conexion.rollback();
				
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("Nombre");
		String tel = resultSet.getString("Telefono");
		String mail =resultSet.getString("mail");
		String fechaNac =resultSet.getString("fechaNac");
		Pair<Integer,String> tipoContacto = new Pair<> (resultSet.getInt("idtipoContacto"),resultSet.getString("tipoContacto"));
		Pair<Integer,String> provincia = new Pair<> (resultSet.getInt("idprovincia"),resultSet.getString("provincia"));
		Pair<Integer,String> localidad = new Pair<> (resultSet.getInt("idlocalidad"),resultSet.getString("localidad"));
		String calle =resultSet.getString("calle");
		String altura =resultSet.getString("altura");
		String piso =resultSet.getString("piso");
		String departamento =resultSet.getString("departamento");
		int idDomicilio = resultSet.getInt("idDomicilio");
		Pair<Integer,String> pais = new Pair<> (resultSet.getInt("idpais"),resultSet.getString("pais"));
		DomicilioDTO domicilio = new DomicilioDTO(idDomicilio, pais, provincia, localidad, calle, altura, piso, departamento);
		int idciudadPreferida = resultSet.getInt("idciudadPreferida");
		String nombreCiudadPreferida = resultSet.getString("ciudadPreferida");
		CiudadPreferidaDTO ciudadPreferida = new CiudadPreferidaDTO(idciudadPreferida, nombreCiudadPreferida);
		
		
//		return new PersonaDTO(id, nombre, tel, mail, fechaNac, tipoContacto, provincia, localidad, calle, altura, piso, departamento);
		return new PersonaDTO(id, nombre, tel, mail, fechaNac, tipoContacto, domicilio, ciudadPreferida);
	}
	
	public boolean update(PersonaDTO personaModificada) {
		PreparedStatement statementReadCiudadFavoritaIsExist;
		PreparedStatement statementReadDomicilioIsExist;
		PreparedStatement statementDomicilio;
		PreparedStatement statementCiudadPreferida;
		PreparedStatement statementPersona;
		PreparedStatement statementLastIdDomicilio;
		PreparedStatement statementLastIdCiudadPreferida;
		PreparedStatement statementIsExistPersona;
		ResultSet resultSetReadCiudadFavoritaIsExist;
		ResultSet resultSetReadDomicilioIsExist;
		ResultSet resultSetLastIdDomicilio;
		ResultSet resultSetLastIdCiudadPreferida;
		ResultSet resultSetIsExistPersona;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try
		{

			statementReadCiudadFavoritaIsExist = conexion.prepareStatement(readCiudadFavoritaIsExist);
			statementReadCiudadFavoritaIsExist.setString(1, personaModificada.getCiudadPreferida().getCiudadPreferida());
			
			resultSetReadCiudadFavoritaIsExist = statementReadCiudadFavoritaIsExist.executeQuery();
			
			if(resultSetReadCiudadFavoritaIsExist.next()){
				statementReadDomicilioIsExist = conexion.prepareStatement(readDomicilioIsExist);
				statementReadDomicilioIsExist.setString(1, personaModificada.getDomicilio().getCalle());
				statementReadDomicilioIsExist.setString(2, personaModificada.getDomicilio().getAltura());
				statementReadDomicilioIsExist.setString(3, personaModificada.getDomicilio().getPiso());
				statementReadDomicilioIsExist.setString(4, personaModificada.getDomicilio().getDepartamento());
				statementReadDomicilioIsExist.setInt(5, personaModificada.getDomicilio().getLocalidad().getKey());
				
				resultSetReadDomicilioIsExist = statementReadDomicilioIsExist.executeQuery();
				
				if(resultSetReadDomicilioIsExist.next()){
					
					statementIsExistPersona = conexion.prepareStatement(readPersonaIsExistOther);
					statementIsExistPersona.setString(1, personaModificada.getNombre());
					statementIsExistPersona.setInt(2, personaModificada.getIdPersona());
					resultSetIsExistPersona = statementIsExistPersona.executeQuery();
					
					if(resultSetIsExistPersona.next()){
						if(!(resultSetIsExistPersona.getInt("cantidad")>=1)){
							statementPersona = conexion.prepareStatement(updatePersona);
							statementPersona.setString(1, personaModificada.getNombre());
							statementPersona.setString(2, personaModificada.getFechaNac());
							statementPersona.setString(3, personaModificada.getTelefono());
							statementPersona.setString(4, personaModificada.getMail());
							statementPersona.setInt(5, personaModificada.getTipoContacto().getKey());
							statementPersona.setInt(6, resultSetReadDomicilioIsExist.getInt("iddomicilio"));
							statementPersona.setInt(7, resultSetReadCiudadFavoritaIsExist.getInt("idciudadPreferida"));
							statementPersona.setInt(8, personaModificada.getIdPersona());
							
							if(statementPersona.executeUpdate() > 0)
							{

								conexion.commit();
								isUpdateExitoso = true;
							}
							else conexion.rollback();
						}
					}
				}else{
					
					statementDomicilio = conexion.prepareStatement(insertDomicilio);
					statementDomicilio.setInt(1, 0);
					statementDomicilio.setString(2, personaModificada.getDomicilio().getCalle());
					statementDomicilio.setString(3, personaModificada.getDomicilio().getAltura());
					statementDomicilio.setString(4, personaModificada.getDomicilio().getPiso());
					statementDomicilio.setString(5, personaModificada.getDomicilio().getDepartamento());
		//			if(resultSetLocalidadxProvinciaxPais.next())			
					statementDomicilio.setInt(6, personaModificada.getDomicilio().getLocalidad().getKey());
		
					if(statementDomicilio.executeUpdate() > 0)
					{
						
						statementIsExistPersona = conexion.prepareStatement(readPersonaIsExistOther);
						statementIsExistPersona.setString(1, personaModificada.getNombre());
						statementIsExistPersona.setInt(2, personaModificada.getIdPersona());
						resultSetIsExistPersona = statementIsExistPersona.executeQuery();
						
						if(resultSetIsExistPersona.next()){
							if(!(resultSetIsExistPersona.getInt("cantidad")>=1)){
								statementPersona = conexion.prepareStatement(updatePersona);
								statementPersona.setString(1, personaModificada.getNombre());
								statementPersona.setString(2, personaModificada.getFechaNac());
								statementPersona.setString(3, personaModificada.getTelefono());
								statementPersona.setString(4, personaModificada.getMail());
								statementPersona.setInt(5, personaModificada.getTipoContacto().getKey());
								
								statementLastIdDomicilio = conexion.prepareStatement(readlastIdDomicilio);
								resultSetLastIdDomicilio = statementLastIdDomicilio.executeQuery();
								if(resultSetLastIdDomicilio.next())
								statementPersona.setInt(6, resultSetLastIdDomicilio.getInt("iddomicilio"));
								
								statementPersona.setInt(7, resultSetReadCiudadFavoritaIsExist.getInt("idciudadPreferida"));
								statementPersona.setInt(8, personaModificada.getIdPersona());
								
								
								if(statementPersona.executeUpdate() > 0)
								{
									
									conexion.commit();
									isUpdateExitoso = true;
								}
								else conexion.rollback();
							}else conexion.rollback();
						}else conexion.rollback();
					}
					else conexion.rollback();
				}
			}else{
				
				statementCiudadPreferida = conexion.prepareStatement(insertCiudadPreferida);
				statementCiudadPreferida.setInt(1, 0);
				statementCiudadPreferida.setString(2, personaModificada.getCiudadPreferida().getCiudadPreferida());
				
				if(statementCiudadPreferida.executeUpdate() > 0)
				{
					statementReadDomicilioIsExist = conexion.prepareStatement(readDomicilioIsExist);
					statementReadDomicilioIsExist.setString(1, personaModificada.getDomicilio().getCalle());
					statementReadDomicilioIsExist.setString(2, personaModificada.getDomicilio().getAltura());
					statementReadDomicilioIsExist.setString(3, personaModificada.getDomicilio().getPiso());
					statementReadDomicilioIsExist.setString(4, personaModificada.getDomicilio().getDepartamento());
					statementReadDomicilioIsExist.setInt(5, personaModificada.getDomicilio().getLocalidad().getKey());
					
					resultSetReadDomicilioIsExist = statementReadDomicilioIsExist.executeQuery();
					
					if(resultSetReadDomicilioIsExist.next()){
						
						statementIsExistPersona = conexion.prepareStatement(readPersonaIsExistOther);
						statementIsExistPersona.setString(1, personaModificada.getNombre());
						statementIsExistPersona.setInt(2, personaModificada.getIdPersona());
						resultSetIsExistPersona = statementIsExistPersona.executeQuery();
						
						if(resultSetIsExistPersona.next()){
							if(!(resultSetIsExistPersona.getInt("cantidad")>=1)){
								statementPersona = conexion.prepareStatement(updatePersona);
								statementPersona.setString(1, personaModificada.getNombre());
								statementPersona.setString(2, personaModificada.getFechaNac());
								statementPersona.setString(3, personaModificada.getTelefono());
								statementPersona.setString(4, personaModificada.getMail());
								statementPersona.setInt(5, personaModificada.getTipoContacto().getKey());
								statementPersona.setInt(6, resultSetReadDomicilioIsExist.getInt("iddomicilio"));
								
								
								statementLastIdCiudadPreferida = conexion.prepareStatement(readlastIdCiudadPreferida);
								resultSetLastIdCiudadPreferida = statementLastIdCiudadPreferida.executeQuery();
								if(resultSetLastIdCiudadPreferida.next())
									statementPersona.setInt(7, resultSetLastIdCiudadPreferida.getInt("idciudadPreferida"));
								
								statementPersona.setInt(8, personaModificada.getIdPersona());
								
								if(statementPersona.executeUpdate() > 0)
								{
									
									conexion.commit();
									isUpdateExitoso = true;
								}
								else conexion.rollback();
							}
						}
					}else{
						
						statementDomicilio = conexion.prepareStatement(insertDomicilio);
						statementDomicilio.setInt(1, 0);
						statementDomicilio.setString(2, personaModificada.getDomicilio().getCalle());
						statementDomicilio.setString(3, personaModificada.getDomicilio().getAltura());
						statementDomicilio.setString(4, personaModificada.getDomicilio().getPiso());
						statementDomicilio.setString(5, personaModificada.getDomicilio().getDepartamento());
			//			if(resultSetLocalidadxProvinciaxPais.next())			
						statementDomicilio.setInt(6, personaModificada.getDomicilio().getLocalidad().getKey());
			
						if(statementDomicilio.executeUpdate() > 0)
						{
							
							statementIsExistPersona = conexion.prepareStatement(readPersonaIsExistOther);
							statementIsExistPersona.setString(1, personaModificada.getNombre());
							statementIsExistPersona.setInt(2, personaModificada.getIdPersona());
							resultSetIsExistPersona = statementIsExistPersona.executeQuery();
							
							if(resultSetIsExistPersona.next()){
								if(!(resultSetIsExistPersona.getInt("cantidad")>=1)){
									statementPersona = conexion.prepareStatement(updatePersona);
									statementPersona.setString(1, personaModificada.getNombre());
									statementPersona.setString(2, personaModificada.getFechaNac());
									statementPersona.setString(3, personaModificada.getTelefono());
									statementPersona.setString(4, personaModificada.getMail());
									statementPersona.setInt(5, personaModificada.getTipoContacto().getKey());
									
									statementLastIdDomicilio = conexion.prepareStatement(readlastIdDomicilio);
									resultSetLastIdDomicilio = statementLastIdDomicilio.executeQuery();
									if(resultSetLastIdDomicilio.next())
									statementPersona.setInt(6, resultSetLastIdDomicilio.getInt("iddomicilio"));;
									
									
									statementLastIdCiudadPreferida = conexion.prepareStatement(readlastIdCiudadPreferida);
									resultSetLastIdCiudadPreferida = statementLastIdCiudadPreferida.executeQuery();
									if(resultSetLastIdCiudadPreferida.next())
										statementPersona.setInt(7, resultSetLastIdCiudadPreferida.getInt("idciudadPreferida"));
									
									statementPersona.setInt(8, personaModificada.getIdPersona());
									
									if(statementPersona.executeUpdate() > 0)
									{
					
										conexion.commit();
										isUpdateExitoso = true;
									}
									else conexion.rollback();
								}else conexion.rollback();
							}else conexion.rollback();
						}
						else conexion.rollback();
					}
				}
				else conexion.rollback();
				
			}
			
			
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isUpdateExitoso;
	}
	
}
