package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoContactoDAO;

public class TipoContactoDAOSQL implements TipoContactoDAO{
	
	private static final String insertTipoContacto = "INSERT INTO tipocontacto(idtipoContacto, tipoContacto) VALUES(?, ?)";
	private static final String delete = "DELETE FROM tipocontacto WHERE tipoContacto = ?";
	private static final String editarTipoContacto = "UPDATE tipocontacto SET tipoContacto = ? WHERE idtipoContacto = ?";
	private static final String readallTipoContacto = "SELECT * FROM tipocontacto";
	
	public boolean insert(String tipoContacto) {
		PreparedStatement statementInsertarTipoContacto;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		
		try
		{
			statementInsertarTipoContacto = conexion.prepareStatement(insertTipoContacto);
			statementInsertarTipoContacto.setInt(1,0);
			statementInsertarTipoContacto.setString(2, tipoContacto);
			if(statementInsertarTipoContacto.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
			else conexion.rollback();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	
	public boolean delete(String tipoContacto_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, tipoContacto_a_eliminar);
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	
	public boolean update(int idTipoContactoAModificar, String tipoContactoModificado) {
		PreparedStatement statementEditarTipoContacto;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditarExitoso = false;
		
		try
		{
			statementEditarTipoContacto = conexion.prepareStatement(editarTipoContacto);
			statementEditarTipoContacto.setString(1,tipoContactoModificado);
			statementEditarTipoContacto.setInt(2, idTipoContactoAModificar);
			if(statementEditarTipoContacto.executeUpdate() > 0)
			{
				conexion.commit();
				isEditarExitoso = true;
			}
			else conexion.rollback();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isEditarExitoso;
	}
	
	
	public List<TipoContactoDTO> readAllTipoContacto()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<TipoContactoDTO> tipoContacto = new ArrayList<TipoContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readallTipoContacto);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				tipoContacto.add(getTipoContactoDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tipoContacto;
	}
	
	private TipoContactoDTO getTipoContactoDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idtipoContacto");
		String nombre = resultSet.getString("tipoContacto");	
		
		return new TipoContactoDTO(id, nombre);
	}
	
}
