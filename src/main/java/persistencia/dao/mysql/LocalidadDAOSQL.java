package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import dto.LocalidadDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.LocalidadDAO;

public class LocalidadDAOSQL implements LocalidadDAO{
	
	private static final String insertLocalidad = "INSERT INTO localidad(idlocalidad, localidad, idprovincia) VALUES(?, ?, ?)";
	private static final String isExistProvincia = "SELECT COUNT(idlocalidad) as cantidad FROM localidad WHERE localidad = ? and idprovincia = ?";
	private static final String delete = "DELETE FROM localidad WHERE idlocalidad = ?";
	private static final String editarLocalidad = "UPDATE localidad SET localidad = ? WHERE idlocalidad = ?";
	private static final String readallLocalidad = "SELECT localidad.idlocalidad, localidad.localidad FROM localidad WHERE localidad.idprovincia = ?";
	
	public boolean insert(String localidad, int Provincia) {
		PreparedStatement statementInsertarLocalidad;
		PreparedStatement statementIsExistLocalidad;
		ResultSet resultSetIsExistLocalidad;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		
		try
		{
			statementIsExistLocalidad = conexion.prepareStatement(isExistProvincia);
			statementIsExistLocalidad.setString(1, localidad);
			statementIsExistLocalidad.setInt(2, Provincia);
			resultSetIsExistLocalidad = statementIsExistLocalidad.executeQuery();
			if(resultSetIsExistLocalidad.next()){
				if(!(resultSetIsExistLocalidad.getInt("cantidad")>=1)){
					
					statementInsertarLocalidad = conexion.prepareStatement(insertLocalidad);
					statementInsertarLocalidad.setInt(1,0);
					statementInsertarLocalidad.setString(2, localidad);
					statementInsertarLocalidad.setInt(3, Provincia);
					if(statementInsertarLocalidad.executeUpdate() > 0)
					{
						conexion.commit();
						isInsertExitoso = true;
					}
					else conexion.rollback();
					
				}
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	
	public boolean delete(int localidad_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, localidad_a_eliminar);
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	
	public boolean update(int idLocalidadAModificar, String localidadModificado, int provinciaLocalidadAModificar) {
		PreparedStatement statementEditarLocalidad;
		PreparedStatement statementIsExistLocalidad;
		ResultSet resultSetIsExistLocalidad;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditarExitoso = false;
		
		try
		{
			statementIsExistLocalidad = conexion.prepareStatement(isExistProvincia);
			statementIsExistLocalidad.setString(1, localidadModificado);
			statementIsExistLocalidad.setInt(2, provinciaLocalidadAModificar);
			resultSetIsExistLocalidad = statementIsExistLocalidad.executeQuery();
			
			if(resultSetIsExistLocalidad.next()){
				if(!(resultSetIsExistLocalidad.getInt("cantidad")>=1)){
					statementEditarLocalidad = conexion.prepareStatement(editarLocalidad);
					statementEditarLocalidad.setString(1,localidadModificado);
					statementEditarLocalidad.setInt(2, idLocalidadAModificar);
					if(statementEditarLocalidad.executeUpdate() > 0)
					{
						conexion.commit();
						isEditarExitoso = true;
					}
					else conexion.rollback();
				}
			}
			
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isEditarExitoso;
	}
	
	
	public List<LocalidadDTO> readAllLocalidad(int Provincia)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<LocalidadDTO> localidad = new ArrayList<LocalidadDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readallLocalidad/* + " where localidadxprovinciaxpais.idprovincia = " + Provincia*/);
			statement.setInt(1,Provincia);
//			statement.setInt(2, Pais);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				localidad.add(getLocalidadDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return localidad;
	}
	
	private LocalidadDTO getLocalidadDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idlocalidad");
		String nombre = resultSet.getString("localidad");
		
		return new LocalidadDTO(id, nombre);
	}

}
