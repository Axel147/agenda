package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.ProvinciaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;

public class ProvinciaDAOSQL implements ProvinciaDAO{
	
	private static final String insertProvincia = "INSERT INTO provincia(idprovincia, provincia, idpais) VALUES(?, ?, ?)";
	private static final String countUsesIdProvinciaxPais = "SELECT COUNT(idprovincia) as cantidad FROM localidad WHERE idprovincia = ?";
	private static final String isExistProvincia = "SELECT COUNT(idprovincia) as cantidad FROM provincia WHERE provincia = ? and idpais = ?";
	private static final String delete = "DELETE FROM provincia WHERE idprovincia = ?";
	private static final String editarProvincia = "UPDATE provincia SET provincia = ? WHERE idprovincia = ?";
	private static final String readallProvincia = "SELECT provincia.idprovincia, provincia.provincia FROM provincia WHERE provincia.idpais = ?";
	
	public boolean insert(String provincia, int pais) {
		PreparedStatement statementInsertarProvincia;
		PreparedStatement statementIsExistProvincia;
		ResultSet resultSetIsExistProvincia;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		
		try
		{
			statementIsExistProvincia = conexion.prepareStatement(isExistProvincia);
			statementIsExistProvincia.setString(1, provincia);
			statementIsExistProvincia.setInt(2, pais);
			resultSetIsExistProvincia = statementIsExistProvincia.executeQuery();
			if(resultSetIsExistProvincia.next()){
				if(!(resultSetIsExistProvincia.getInt("cantidad")>=1)){
					statementInsertarProvincia = conexion.prepareStatement(insertProvincia);
					statementInsertarProvincia.setInt(1,0);
					statementInsertarProvincia.setString(2, provincia);
					statementInsertarProvincia.setInt(3,pais);
					if(statementInsertarProvincia.executeUpdate() > 0)
					{
						conexion.commit();
						isInsertExitoso = true;
					}else conexion.rollback();
				}
			}
			
			
//			else conexion.rollback();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	
	public boolean delete(int provincia_a_eliminar) {
		PreparedStatement statement;
		PreparedStatement statementCountUsesIdProvincia;
		ResultSet resultSetCountUsesIdProvincia;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{		
			statementCountUsesIdProvincia = conexion.prepareStatement(countUsesIdProvinciaxPais);
			statementCountUsesIdProvincia.setInt(1, provincia_a_eliminar);
			resultSetCountUsesIdProvincia = statementCountUsesIdProvincia.executeQuery();
			if(resultSetCountUsesIdProvincia.next()){
				if(!(resultSetCountUsesIdProvincia.getInt("cantidad")>=1)){
					statement = conexion.prepareStatement(delete);
					statement.setInt(1, provincia_a_eliminar);
					if(statement.executeUpdate() > 0)
					{
						conexion.commit();
						isdeleteExitoso = true;
					}
				}
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	
	public boolean update(int idProvinciaAModificar, String provinciaModificado, int paisProvinciaAModificar) {
		PreparedStatement statementEditarProvincia;
		PreparedStatement statementIsExistProvincia;
		ResultSet resultSetIsExistProvincia;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditarExitoso = false;
		
		try
		{
			statementIsExistProvincia = conexion.prepareStatement(isExistProvincia);
			statementIsExistProvincia.setString(1, provinciaModificado);
			statementIsExistProvincia.setInt(2, paisProvinciaAModificar);
			resultSetIsExistProvincia = statementIsExistProvincia.executeQuery();
			
			if(resultSetIsExistProvincia.next()){
				if(!(resultSetIsExistProvincia.getInt("cantidad")>=1)){
					statementEditarProvincia = conexion.prepareStatement(editarProvincia);
					statementEditarProvincia.setString(1,provinciaModificado);
					statementEditarProvincia.setInt(2, idProvinciaAModificar);
					if(statementEditarProvincia.executeUpdate() > 0)
					{
						conexion.commit();
						isEditarExitoso = true;
					}
					else conexion.rollback();
				}
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isEditarExitoso;
	}
	
	public List<ProvinciaDTO> readAllProvincia(int Pais)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ProvinciaDTO> provincia = new ArrayList<ProvinciaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readallProvincia /*+ " where localidadxprovinciaxpais.idpais = " + Pais*/);
			statement.setInt(1, Pais);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				provincia.add(getProvinciaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return provincia;
	}
	
	private ProvinciaDTO getProvinciaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idprovincia");
		String nombre = resultSet.getString("provincia");
	
		return new ProvinciaDTO(id, nombre);
	}
	
}
