package persistencia.dao.interfaz;

import java.util.List;

import dto.LocalidadDTO;

public interface LocalidadDAO {

	public List<LocalidadDTO> readAllLocalidad(int Provincia);
	
	public boolean insert(String localidad, int idprovincia);

	public boolean delete(int localidad_a_eliminar);
	
	public boolean update(int idLocalidadAModificar,String localidadModificada, int provinciaLocalidadAModificar);
	
}
