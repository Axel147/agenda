package persistencia.dao.interfaz;

import java.util.List;


import dto.TipoContactoDTO;

public interface TipoContactoDAO {
	
	public List<TipoContactoDTO> readAllTipoContacto();
	
	public boolean insert(String tipoContacto);

	public boolean delete(String tipoContacto_a_eliminar);
	
	public boolean update(int idTipoContactoAModificar,String tipoContactoModificado);
	
}
