package persistencia.dao.interfaz;

import java.util.List;


import dto.ProvinciaDTO;
import dto.TipoContactoDTO;

public interface ProvinciaDAO {
	
	public List<ProvinciaDTO> readAllProvincia(int Pais);
	
	public boolean insert(String provincia, int pais);

	public boolean delete(int provincia_a_eliminar);
	
	public boolean update(int idProvinciaAModificar,String provinciaModificado, int paisProvinciaAModificar);
	
}
