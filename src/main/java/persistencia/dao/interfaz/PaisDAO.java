package persistencia.dao.interfaz;

import java.util.List;


import dto.PaisDTO;

public interface PaisDAO {

	public List<PaisDTO> readAllPais();
	
	public boolean insert(String pais);

	public boolean delete(int pais_a_eliminar);
	
	public boolean update(int idPaisAModificar,String paisModificado);
	
}
