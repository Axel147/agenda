package dto;

public class CiudadPreferidaDTO {
	private int idCiudadPreferida;
	private String ciudadPreferida;

	public CiudadPreferidaDTO(int idCiudadPreferida, String ciudadPreferida)
	{
		this.idCiudadPreferida = idCiudadPreferida;
		this.ciudadPreferida = ciudadPreferida;
	}
	
	public int getIdCiudadPreferida() 
	{
		return this.idCiudadPreferida;
	}

	public void setIdCiudadPreferida(int idCiudadPreferida) 
	{
		this.idCiudadPreferida = idCiudadPreferida;
	}

	public String getCiudadPreferida() 
	{
		return this.ciudadPreferida;
	}

	public void setCiudadPreferida(String ciudadPreferida) 
	{
		this.ciudadPreferida = ciudadPreferida;
	}
	
	@Override
    public String toString(){
        return this.ciudadPreferida;
    }
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((CiudadPreferidaDTO)objeto).ciudadPreferida.equals(this.ciudadPreferida)&&((CiudadPreferidaDTO)objeto).idCiudadPreferida == (this.idCiudadPreferida)){
            return true;
        }
        return false;
    }
}
