package dto;

import javafx.util.Pair;

public class DomicilioDTO {
	
	private int idDomicilio;
	private Pair<Integer,String> pais;
	private Pair<Integer,String> provincia;
	private Pair<Integer,String> localidad;
	private String calle;
	private String altura;
	private String piso;
	private String departamento;
	
	public DomicilioDTO(int idDomicilio, Pair pais, Pair provincia, Pair localidad, String calle, String altura, String piso, String departamento)
	{
		this.idDomicilio = idDomicilio;
		this.pais = pais;
		this.provincia = provincia;
		this.localidad = localidad;
		this.calle = calle;
		this.altura = altura;
		this.piso = piso;
		this.departamento = departamento;
	}

	public int getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(int idDomicilio) {
		this.idDomicilio = idDomicilio;
	}

	public Pair<Integer, String> getPais() {
		return pais;
	}

	public void setPais(Pair<Integer, String> pais) {
		this.pais = pais;
	}

	public Pair<Integer, String> getProvincia() {
		return provincia;
	}

	public void setProvincia(Pair<Integer, String> provincia) {
		this.provincia = provincia;
	}

	public Pair<Integer, String> getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Pair<Integer, String> localidad) {
		this.localidad = localidad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	
	
}
