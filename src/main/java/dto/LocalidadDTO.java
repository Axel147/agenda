package dto;

public class LocalidadDTO {
	private int idLocalidad;
	private String localidad;
	private int provincia;

	public LocalidadDTO(int idLocalidad, String localidad/*, int provincia*/)
	{
		this.idLocalidad = idLocalidad;
		this.localidad = localidad;
//		this.provincia = provincia;
	}
	
	public int getIdLocalidad() 
	{
		return this.idLocalidad;
	}

	public void setIdLocalidad(int idLocalidad) 
	{
		this.idLocalidad = idLocalidad;
	}

	public String getLocalidad() 
	{
		return this.localidad;
	}

	public void setLocalidad(String localidad) 
	{
		this.localidad = localidad;
	}
	
	public int getProvincia() {
		return provincia;
	}

	public void setProvincia(int provincia) {
		this.provincia = provincia;
	}

	@Override
    public String toString(){
        return this.localidad;
    }
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((LocalidadDTO)objeto).localidad.equals(this.localidad)&&((LocalidadDTO)objeto).idLocalidad == (this.idLocalidad)){
            return true;
        }
        return false;
    }
}
