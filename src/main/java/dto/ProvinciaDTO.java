package dto;

public class ProvinciaDTO {
	
	private int idProvincia;
	private String provincia;
	private int pais;

	public ProvinciaDTO(int idProvincia, String provincia/*, int pais*/)
	{
		this.idProvincia = idProvincia;
		this.provincia = provincia;
//		this.pais = pais;
	}
	
	public int getIdProvincia() 
	{
		return this.idProvincia;
	}

	public void setIdProvincia(int idProvincia) 
	{
		this.idProvincia = idProvincia;
	}

	public String getProvincia() 
	{
		return this.provincia;
	}

	public void setProvincia(String provincia) 
	{
		this.provincia = provincia;
	}
	
	public int getPais() {
		return pais;
	}

	public void setPais(int pais) {
		this.pais = pais;
	}

	@Override
    public String toString(){
        return this.provincia;
    }
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((ProvinciaDTO)objeto).provincia.equals(this.provincia)&&((ProvinciaDTO)objeto).idProvincia == (this.idProvincia)){
            return true;
        }
        return false;
    }
	
}
