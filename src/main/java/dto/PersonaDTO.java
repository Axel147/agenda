package dto;



import java.util.ArrayList;
import java.util.List;

import javafx.util.Pair;

public class PersonaDTO
{
	private int idPersona;
	private String nombre;
	private String telefono;
	private String mail;
	private String fechaNac;
	private Pair<Integer,String> tipoContacto;
	private DomicilioDTO domicilio;
	private CiudadPreferidaDTO ciudadPreferida;
//	private Pair<Integer,String> provincia;
//	private Pair<Integer,String> localidad;
//	private String calle;
//	private String altura;
//	private String piso;
//	private String departamento;

	public PersonaDTO(int idPersona, String nombre, String telefono,String mail,String fechaNac,Pair<Integer,String> tipoContacto, DomicilioDTO domicilio, CiudadPreferidaDTO ciudadPreferida/*, Pair<Integer,String> provincia, Pair<Integer,String> localidad, String calle, String altura, String piso, String departamento*/)
	{
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this.mail = mail;
		this.fechaNac = fechaNac;
		this.tipoContacto = tipoContacto;
		this.domicilio = domicilio;
		this.ciudadPreferida = ciudadPreferida;
//		this.provincia = provincia;
//		this.localidad = localidad;
//		this.calle = calle;
//		this.altura = altura;
//		this.piso = piso;
//		this.departamento = departamento;
	}
	
	public int getIdPersona() 
	{
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getFechaNac() {
		return fechaNac;
	}

	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}

	public Pair<Integer,String> getTipoContacto() {
		return tipoContacto;
	}

	public void setTipoContacto(Pair<Integer,String> tipoContacto) {
		this.tipoContacto = tipoContacto;
	}

	public DomicilioDTO getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(DomicilioDTO domicilio) {
		this.domicilio = domicilio;
	}

	public CiudadPreferidaDTO getCiudadPreferida() {
		return ciudadPreferida;
	}

	public void setCiudadPreferida(CiudadPreferidaDTO ciudadPreferida) {
		this.ciudadPreferida = ciudadPreferida;
	}
	
	
	
//	public Pair<Integer,String> getProvincia() {
//		return provincia;
//	}
//
//	public void setProvincia(Pair<Integer,String> provincia) {
//		this.provincia = provincia;
//	}
//
//	public Pair<Integer,String> getLocalidad() {
//		return localidad;
//	}
//
//	public void setLocalidad(Pair<Integer,String> localidad) {
//		this.localidad = localidad;
//	}
//
//	public String getCalle() {
//		return calle;
//	}
//
//	public void setCalle(String calle) {
//		this.calle = calle;
//	}
//
//	public String getAltura() {
//		return altura;
//	}
//
//	public void setAltura(String altura) {
//		this.altura = altura;
//	}
//
//	public String getPiso() {
//		return piso;
//	}
//
//	public void setPiso(String piso) {
//		this.piso = piso;
//	}
//
//	public String getDepartamento() {
//		return departamento;
//	}
//
//	public void setDepartamento(String departamento) {
//		this.departamento = departamento;
//	}
	
	
	
}
