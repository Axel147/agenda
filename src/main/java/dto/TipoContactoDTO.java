package dto;

public class TipoContactoDTO {
	private int idTipoContacto;
	private String tipoContacto;

	public TipoContactoDTO(int idTipoContacto, String tipoContacto)
	{
		this.idTipoContacto = idTipoContacto;
		this.tipoContacto = tipoContacto;
	}
	
	public int getIdTipoContacto() 
	{
		return this.idTipoContacto;
	}

	public void setIdTipoContacto(int idTipoContacto) 
	{
		this.idTipoContacto = idTipoContacto;
	}

	public String getTipoContacto() 
	{
		return this.tipoContacto;
	}

	public void setTipoContacto(String tipoContacto) 
	{
		this.tipoContacto = tipoContacto;
	}
	
	@Override
    public String toString(){
        return this.tipoContacto;
    }
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((TipoContactoDTO)objeto).tipoContacto.equals(this.tipoContacto)&&((TipoContactoDTO)objeto).idTipoContacto == (this.idTipoContacto)){
            return true;
        }
        return false;
    }
}
