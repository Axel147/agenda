package modelo;

import java.util.List;

import dto.PaisDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.PaisDAO;

public class Pais {
	
	private PaisDAO pais;
	
	public Pais (DAOAbstractFactory metodo_persistencia)
	{
		this.pais = metodo_persistencia.createPaisDAO();
	}
	
	public List<PaisDTO> obtenerPais()
	{
		return this.pais.readAllPais();		
	}
	
	public void agregarPais(String nuevoPais) {
		this.pais.insert(nuevoPais);
	}
	
	public void editarPais(int idPaisAModificar,String PaisModificado) {
		this.pais.update(idPaisAModificar,PaisModificado);
	}
	
	public boolean borrarPais(int Pais_a_eliminar) 
	{
		return this.pais.delete(Pais_a_eliminar);
	}
}
