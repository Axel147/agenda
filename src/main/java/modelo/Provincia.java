package modelo;

import java.util.List;


import dto.ProvinciaDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.ProvinciaDAO;

public class Provincia {

private ProvinciaDAO provincia;
	
	public Provincia (DAOAbstractFactory metodo_persistencia)
	{
		this.provincia = metodo_persistencia.createProvinciaDAO();
	}
	
	public List<ProvinciaDTO> obtenerProvincia(int Pais)
	{
		return this.provincia.readAllProvincia(Pais);		
	}
	
	public boolean agregarProvincia(String nuevaProvincia, int pais) {
		return this.provincia.insert(nuevaProvincia, pais);
	}
	
	public boolean editarProvincia(int idProvinciaAModificar,String ProvinciaModificado, int paisProvinciaAModificar) {
		return this.provincia.update(idProvinciaAModificar,ProvinciaModificado, paisProvinciaAModificar);
	}
	
	public boolean borrarProvincia(int provincia_a_eliminar) 
	{
		return this.provincia.delete(provincia_a_eliminar);
	}
	
}
