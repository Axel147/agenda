package modelo;

import java.util.List;

import dto.LocalidadDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.LocalidadDAO;

public class Localidad {
	
	private LocalidadDAO localidad;
	
	public Localidad (DAOAbstractFactory metodo_persistencia)
	{
		this.localidad = metodo_persistencia.createLocalidadDAO();
	}
	
	public List<LocalidadDTO> obtenerLocalidad(int Provincia)
	{
		return this.localidad.readAllLocalidad(Provincia);		
	}
	
	public boolean agregarLocalidad(String nuevaLocalidad, int idprovincia) {
		return this.localidad.insert(nuevaLocalidad, idprovincia);
	}
	
	public boolean editarLocalidad(int idLocalidadAModificar,String LocalidadModificada, int provinciaLocalidadAModificar) {
		return this.localidad.update(idLocalidadAModificar,LocalidadModificada, provinciaLocalidadAModificar);
	}
	
	public boolean borrarLocalidad(int localidad_a_eliminar) 
	{
		return this.localidad.delete(localidad_a_eliminar);
	}
}
