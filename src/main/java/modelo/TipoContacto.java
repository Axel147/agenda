package modelo;

import java.util.List;

import dto.PersonaDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.TipoContactoDAO;

public class TipoContacto {
	private TipoContactoDAO tipoContacto;
	
	public TipoContacto (DAOAbstractFactory metodo_persistencia)
	{
		this.tipoContacto = metodo_persistencia.createTipoContactoDAO();
	}
	
	public List<TipoContactoDTO> obtenerTiposContacto()
	{
		return this.tipoContacto.readAllTipoContacto();		
	}
	
	public void agregarTipoDeContacto(String nuevoTipoDeContacto) {
		this.tipoContacto.insert(nuevoTipoDeContacto);
	}
	
	public void editarTipoContacto(int idTipoContactoAModificar,String tipoContactoModificado) {
		this.tipoContacto.update(idTipoContactoAModificar,tipoContactoModificado);
	}
	
	public boolean borrarTipoContacto(String tipoContacto_a_eliminar) 
	{
		return this.tipoContacto.delete(tipoContacto_a_eliminar);
	}
}
