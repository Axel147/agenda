-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: agenda
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `domicilio`
--

DROP TABLE IF EXISTS `domicilio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domicilio` (
  `iddomicilio` int(11) NOT NULL AUTO_INCREMENT,
  `calle` varchar(45) DEFAULT NULL,
  `altura` varchar(45) DEFAULT NULL,
  `piso` varchar(45) DEFAULT NULL,
  `departamento` varchar(45) DEFAULT NULL,
  `idlocalidad` int(11) NOT NULL,
  PRIMARY KEY (`iddomicilio`),
  KEY `fk_domicilio_localidad_idx` (`idlocalidad`),
  CONSTRAINT `fk_domicilio_localidad` FOREIGN KEY (`idlocalidad`) REFERENCES `localidad` (`idlocalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domicilio`
--

LOCK TABLES `domicilio` WRITE;
/*!40000 ALTER TABLE `domicilio` DISABLE KEYS */;
INSERT INTO `domicilio` VALUES (1,'Calle Falsa','123','5','3',4),(2,'Calle falsa','621','1','1',4),(3,'Calle Verdadera','245','3','2',1),(19,'Gral. Chubut2','11','11','111',22),(20,'231','123','111','44',26),(21,'444','123','111','44',10),(22,'Gral. Chubut','11','11','111',22);
/*!40000 ALTER TABLE `domicilio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localidad`
--

DROP TABLE IF EXISTS `localidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localidad` (
  `idlocalidad` int(11) NOT NULL AUTO_INCREMENT,
  `localidad` varchar(45) NOT NULL,
  `idprovincia` int(11) NOT NULL,
  PRIMARY KEY (`idlocalidad`),
  KEY `fk_localidad_provincia_idx` (`idprovincia`),
  CONSTRAINT `fk_localidad_provincia` FOREIGN KEY (`idprovincia`) REFERENCES `provincia` (`idprovincia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localidad`
--

LOCK TABLES `localidad` WRITE;
/*!40000 ALTER TABLE `localidad` DISABLE KEYS */;
INSERT INTO `localidad` VALUES (1,'Hurlingham‎',1),(2,'Morón',1),(3,'José C. Paz',1),(4,'San Miguel',1),(5,'Achiras',5),(6,'Acebal',20),(7,'Bella Vista',6),(8,'Primero de Mayo',7),(9,'Pozo del Tigre',8),(10,'Charata',3),(11,'Barrios',9),(12,'Rivadavia',16),(13,'Casares',21),(14,'Cerro Negro',2),(15,'Olta',11),(16,'Bermejo',17),(17,'Las Heras',12),(18,'La Calera',18),(19,'Arata',10),(20,'Añelo',14),(21,'Arroyo Los Berros',15),(22,'Buen Pasto',4),(23,'Hipólito Yrigoyen',19),(24,'Rio grande',22),(25,'San Miguel',23),(26,'Bella Vista',1),(27,'Bella Vista',17),(28,'Primero de Mayo',13),(29,'Coari',24),(30,'Maricá',25),(31,'Mesquita',25),(32,'Borba',24),(33,'Bremer',26);
/*!40000 ALTER TABLE `localidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pais` (
  `idpais` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(45) NOT NULL,
  PRIMARY KEY (`idpais`),
  UNIQUE KEY `pais_UNIQUE` (`pais`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'Argentina'),(5,'Brazil');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personas` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `fechaNac` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `tipo_contacto` int(11) NOT NULL,
  `domicilio` int(11) NOT NULL,
  PRIMARY KEY (`idpersona`),
  KEY `fk_person_tipocontacto_idx` (`tipo_contacto`),
  KEY `fk_person_domicilio_idx` (`domicilio`),
  CONSTRAINT `fk_person_domicilio` FOREIGN KEY (`domicilio`) REFERENCES `domicilio` (`iddomicilio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_tipocontacto` FOREIGN KEY (`tipo_contacto`) REFERENCES `tipocontacto` (`idtipoContacto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (1,'Juan Perez','2020-9-07','124565763','juan.topo@gmail.com',3,1),(2,'Componente axel','2020-9-03','1664996578','ca@gmail.com',3,2),(3,'Pedro SinNombre','2020-9-13','152203688','pedrosn@gmail.com',1,3),(4,'Pepe Rodriguez','2020-9-26','1258794316','pepeRodriguez@gmail.com22',3,22);
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provincia`
--

DROP TABLE IF EXISTS `provincia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provincia` (
  `idprovincia` int(11) NOT NULL AUTO_INCREMENT,
  `provincia` varchar(45) NOT NULL,
  `idpais` int(11) NOT NULL,
  PRIMARY KEY (`idprovincia`),
  KEY `fk_provincias2_pais_idx` (`idpais`),
  CONSTRAINT `fk_provincias2_pais` FOREIGN KEY (`idpais`) REFERENCES `pais` (`idpais`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provincia`
--

LOCK TABLES `provincia` WRITE;
/*!40000 ALTER TABLE `provincia` DISABLE KEYS */;
INSERT INTO `provincia` VALUES (1,'Buenos Aires',1),(2,'Catamarca',1),(3,'Chaco',1),(4,'Chubut',1),(5,'Córdoba',1),(6,'Corrientes',1),(7,'Entre Ríos',1),(8,'Formosa',1),(9,'Jujuy',1),(10,'La Pampa',1),(11,'La Rioja',1),(12,'Mendoza',1),(13,'Misiones',1),(14,'Neuquén',1),(15,'Río Negro',1),(16,'Salta',1),(17,'San Juan',1),(18,'San Luis',1),(19,'Santa Cruz',1),(20,'Santa Fe',1),(21,'Santiago del Estero',1),(22,'Tierra del Fuego',1),(23,'Tucumán',1),(24,'Amazonas',5),(25,'Rio de Janeiro',5),(26,'Santa Catarina',5);
/*!40000 ALTER TABLE `provincia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipocontacto`
--

DROP TABLE IF EXISTS `tipocontacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipocontacto` (
  `idtipoContacto` int(11) NOT NULL AUTO_INCREMENT,
  `tipoContacto` varchar(45) NOT NULL,
  PRIMARY KEY (`idtipoContacto`),
  UNIQUE KEY `tipoContacto_UNIQUE` (`tipoContacto`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipocontacto`
--

LOCK TABLES `tipocontacto` WRITE;
/*!40000 ALTER TABLE `tipocontacto` DISABLE KEYS */;
INSERT INTO `tipocontacto` VALUES (3,'Amigos'),(2,'Familia'),(1,'Trabajo');
/*!40000 ALTER TABLE `tipocontacto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-18  5:02:37
