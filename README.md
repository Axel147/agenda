# Laboratorio de Construcción de Software

## INFORME PROYECTO AGENDA 

1.**[INTRODUCCIÓN](https://gitlab.com/erik.argel/trabajo-practico-inicial-pp1#introducción)** 

2.**[IMPLEMENTACIÓN AGENDA](https://gitlab.com/erik.argel/trabajo-practico-inicial-pp1#implementación-agenda)**

3.**[PROBLEMA PRINCIPAL Y SOLUCIÓN](https://gitlab.com/erik.argel/trabajo-practico-inicial-pp1#problema-principal-y-solución)**


### INTRODUCCIÓN
En el presente informe se presentan los detalles sobre la funcionalidad actual de la agenda solicitada por la materia. Se detallarán las funciones que realizan las ventanas implementadas y las clases más relevantes. El objetivo de este documento es mostrar los avances alcanzados hasta el momento y señalar el problema principal que surgió así como su respectiva solución.

### Implementación Agenda
En primer lugar nos encontramos con la ventana inicial de la agenda, creada desde la clase Vista. Esta se encarga de mostrar los contactos con los que contamos actualmente en nuestra agenda detallando sus correspondientes datos (Nombre y apellido, teléfono, email, Fecha de nacimiento, tipo de contacto, país, provincia , localidad, calle , altura,  piso y departamento).
    
![1](ImagenesInforme/1.jpg)
![2](ImagenesInforme/2.jpg)

Como se puede ver la ventana cuenta con los botones correspondientes para realizar una alta,modificacion y eliminacion de los contactos además de tener un botón de reporte, los cuales se nos fueron proporcionados. Al presionar el botón “agregar” el sistema nos dirigirá a la ventana “Agregar persona” (implementada por la clase VentanaPersona). Esta cuenta con el formulario correspondiente para la creación de un contacto nuevo. Sumado a esto debemos destacar la aparición de los botones de personalización para las distintas listas desplegables que poseemos (Tipo contacto, País, Provincia, Localidad) .

El funcionamiento básico de los botones mencionados anteriormente es similar, al seleccionar el botón de personalización de tipo de contacto nos abrirá una ventana que mostrará la lista de los tipo de contactos actuales.

![3](ImagenesInforme/3.jpg)

De igual manera que en la ventana anterior contamos con la botones para realizar el abm correspondiente. Presionar el botón “Agregar” abrirá una ventana adicional (implementada por la clase “VentanaABMTerritorio”) que solicitará el nombre del tipo de contacto nuevo y al presionar “Guardar” este se almacenará en la base de datos. 

![4](ImagenesInforme/4.jpg)

Regresando a la ventana anterior, al presionar el botón Editar no ocurrirá nada si es que no tenemos un Tipo de contacto seleccionado. En caso contrario, abrirá la ventana de edición (la misma que se utiliza para agregar un tipo de contacto nuevo) pero esta vez el campo de nombre contendrá el nombre del tipo de contacto seleccionado, pudiendo cambiarlo o no, y estos cambios serán guardados luego de presionar “Guardar”.

Similar a como se realiza la edición del tipo de contacto se elimina uno, seleccionamos un tipo de contacto y se presiona “eliminar”. De esta manera si el tipo de contacto no está siendo utilizado por algún Contacto se eliminará correctamente, en caso contrario no sucederá (en la versión actual no se captura esta opción, por lo que en la terminal figuran los distintos errores).

Como se mencionó con anterioridad, los botones de personalización de las listas desplegables funcionan de manera similar, abriendo una ventana que muestra una lista con los ítems a modificar que se poseen en ese momento y su respectivo ABM. Para personalizar país se utiliza la ventana VentanaABMPais” que al querer ingresar un pais nuevo abre una ventana que solicita el nombre del pais y luego se guarda, para personalizar provincias  usamos la clase “VentanaGeneralABMProvincia” que al ingresar a “agregar” abre otra ventana implementada por la clase “VentanaCargaProvincia”,por último para las localidades usamos “VentanaGeneralABMLocalidad” y al agregar abre otra ventana del tipo“VentanaCargaLocalidad”, es necesario mencionar que estas últimas dos ventanas cuentan con sus listas desplegables propias, para “VentanaCargaProvincia” existe una lista que referencia a los países actuales que tenemos almacenados, por lo que para ingresar una provincia deberemos seleccionar de la lista el país al que pertenece, colocar el nombre de la provincia y luego guardar. Para “VentanaCargaLocalidad” tendremos dos listas una con los países actuales y otra con las provincias siguiendo la idea anterior. 

![5](ImagenesInforme/5.jpg)![6](ImagenesInforme/6.jpg)
![7](ImagenesInforme/7.jpg)![8](ImagenesInforme/8.jpg)

Continuando con la sección de abm de los contactos al seleccionar “editar” y tener seleccionado un contacto este abre una ventana igual a la utilizada en el momento de agregar, pero que contiene los campos del contacto actual para poder ser luego modificados y almacenados. Eliminar funciona de igual manera que en la eliminación de tipo de contacto, eliminando el contacto al presionar el botón y tener previamente seleccionado un contacto.
En lo que respecta a la sección de reporte, al tocar el botón “Reporte” ubicado en la ventana principal, nos abrira otra ventana que nos permitirá elegir el tipo de reporte a generar (nos permite elegir más de un reporte a la vez). Luego de esto nos abrirá otra ventana con el reporte de los contactos a generar.

![9](ImagenesInforme/9.jpg)
![10](ImagenesInforme/10.jpg)

### Problema principal y solución

Debido a no haber contemplado algunos altercados que habría en el diseño del sistema fueron necesarios dos cambios en la implementación de la base de datos, esto desembocando en la modificación de una parte del código en ambas ocasiones. El primer cambio surgió debido a una triple relación que teníamos entre las tablas país, provincia y localidad, notamos que no podíamos ingresar una provincia sin ingresar una localidad, por lo que se procedió a realizar este primer cambio. Posteriormente luego de editar el código y proceder a modificar las query se podía realizar el ingresar pero seguía conteniendo cierta complejidad, además de que al querer realizar una edición de una provincia debido a la implementación utilizada estaremos editando la provincia que está usando otro país, esto produjo un segundo cambio que permitiera evitar este problema. 
